package app.virtualrace.widget.radio

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.RadioButton
import androidx.core.content.ContextCompat
import androidx.core.view.children
import app.virtualrace.R
import app.virtualrace.core.extension.weak

typealias AndroidRadioGroup = android.widget.RadioGroup

class RadioGroup : AndroidRadioGroup {

    private val indicator: Drawable?

    private val radioButtons: Sequence<RadioButton> get() = children.mapNotNull { it as? RadioButton }

    private var lastCheckedRadioButtonId = checkedRadioButtonId

    private var currentTranslationX = 0f

    private var animator: ValueAnimator? = null

    private var checkedListener: OnCheckedChangeListener? = null
    private var internalCheckedListener = OnCheckedChangeListener { group, checkedId ->
        checkedListener?.onCheckedChanged(group, checkedId)
        animateCheckedChange()
        lastCheckedRadioButtonId = checkedId
    }.also { super.setOnCheckedChangeListener(it) }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.RadioGroup)

        val indicatorResId = ta.getResourceId(R.styleable.RadioGroup_selectionIndicator, 0)
        if (indicatorResId > 0) {
            indicator = ContextCompat.getDrawable(context, indicatorResId)
        } else {
            indicator = null
        }

        ta.recycle()

        setWillNotDraw(indicator == null)
    }

    override fun setOnCheckedChangeListener(listener: OnCheckedChangeListener?) {
        checkedListener = listener
        super.setOnCheckedChangeListener(internalCheckedListener)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val radioButtons = radioButtons

        val indicator = indicator
        val count = radioButtons.count()
        val index = radioButtons.indexOfFirst { it.id == checkedRadioButtonId }

        if (indicator == null || count == 0 || index < 0) return

        canvas.save()
        canvas.translate(currentTranslationX, 0f)

        indicator.setBounds(0, 0, getChildAt(index).width, height)
        indicator.draw(canvas)

        canvas.restore()
    }

    private fun animateCheckedChange() {
        animator?.cancel()
        animator = ValueAnimator.ofFloat(0f, 1f).apply {
            addUpdateListener(
                CheckedChangeAnimationListener(lastCheckedRadioButtonId, checkedRadioButtonId, this@RadioGroup)
            )
            start()
        }
    }

    internal class CheckedChangeAnimationListener(
        private val fromId: Int,
        private val toId: Int,
        group: RadioGroup
    ) : ValueAnimator.AnimatorUpdateListener {
        private val weakGroup = group.weak()
        private val group get() = weakGroup.get()

        override fun onAnimationUpdate(animation: ValueAnimator) {
            val fromView = group?.findViewById<View>(fromId)
            val toView = group?.findViewById<View>(toId)

            val fromTranslation = fromView?.left?.toFloat() ?: 0f
            val toTranslation = toView?.left?.toFloat() ?: 0f

            val progress = animation.animatedValue as? Float ?: 0f

            group?.apply {
                currentTranslationX = fromTranslation + (toTranslation - fromTranslation) * progress
                invalidate()
            }
        }
    }
}