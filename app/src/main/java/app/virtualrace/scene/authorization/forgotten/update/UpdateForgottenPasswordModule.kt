package app.virtualrace.scene.authorization.forgotten.update

import app.virtualrace.core.di.module.CoroutineModule

class UpdateForgottenPasswordModule(f: UpdateForgottenPasswordFragment): CoroutineModule(f) {
    init {
        bind(UpdateForgottenPasswordScene.View::class.java).toInstance(f)
        bind(UpdateForgottenPasswordScene.Presenter::class.java).to(UpdateForgottenPasswordPresenter::class.java).singletonInScope()
    }
}