package app.virtualrace.scene.authorization.signup

import app.virtualrace.core.di.module.CoroutineModule

class SignUpModule(f: SignUpFragment) : CoroutineModule(f) {
    init {
        bind(SignUpScene.View::class.java).toInstance(f)
        bind(SignUpScene.Presenter::class.java).to(SignUpPresenter::class.java).singletonInScope()
        bind(SignUpScene.Collector::class.java).toInstance(SignUpCollector())
    }
}