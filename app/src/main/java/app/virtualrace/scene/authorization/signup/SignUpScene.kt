package app.virtualrace.scene.authorization.signup

import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.data.Result

import app.virtualrace.data.api.model.UserRegister

interface SignUpScene {
    interface View: Attachable, Messageable
    interface Presenter {
        fun signUp()
    }

    interface Collector {
        fun collect(view: View): Result<UserRegister>
    }
}