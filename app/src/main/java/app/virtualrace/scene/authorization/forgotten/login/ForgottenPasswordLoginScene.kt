package app.virtualrace.scene.authorization.forgotten.login

import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.data.Result
import app.virtualrace.data.api.model.UserLogin
import app.virtualrace.scene.authorization.signin.SignInScene

interface ForgottenPasswordLoginScene {
    interface View: Attachable, Messageable
    interface Presenter {
        fun login()
    }

    interface Collector {
        fun collect(v: View): Result<UserLogin>
    }
}