package app.virtualrace.scene.authorization

import app.virtualrace.core.common.internal.LifecycleAwareNavigationHolder
import app.virtualrace.core.presentation.router.FlowRouter
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class AuthorizationModule(f: AuthorizationFragment, router: Router) : Module() {
    init {
        // Flow
        val cicerone = Cicerone.create(FlowRouter(router))
        bind(FlowRouter::class.java).toInstance(cicerone.router)
        bind(LifecycleAwareNavigationHolder::class.java)
            .toInstance(LifecycleAwareNavigationHolder(cicerone.navigatorHolder))

        bind(AuthorizationScene.View::class.java).toInstance(f)
        bind(AuthorizationScene.Presenter::class.java)
            .to(AuthorizationPresenter::class.java)
            .singletonInScope()
    }
}