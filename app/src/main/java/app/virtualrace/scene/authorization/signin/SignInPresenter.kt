package app.virtualrace.scene.authorization.signin

import app.virtualrace.R
import app.virtualrace.account.Account
import app.virtualrace.account.AccountManager
import app.virtualrace.common.Constant.STATUS_ERROR
import app.virtualrace.common.Constant.STATUS_OK
import app.virtualrace.core.common.ExceptionHandler
import app.virtualrace.core.common.ExceptionHumanizer
import app.virtualrace.core.common.Executor
import app.virtualrace.core.common.ResourceManager
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.ProgressManager
import app.virtualrace.core.presentation.presenter.ExceptionHandlingPresenter
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.data.Result
import app.virtualrace.data.api.Api
import app.virtualrace.scene.Screen
import app.virtualrace.util.InternetConnection
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class SignInPresenter @Inject constructor(
    v: SignInScene.View,
    job: Job,
    executor: Executor,
    humanizer: ExceptionHumanizer,
    resourceManager: ResourceManager,
    private val router: FlowRouter,
    private val api: Api,
    private val accountManager: AccountManager,
    private val progressManager: ProgressManager,
    private val collector: SignInScene.Collector,
    private val internetConnection: InternetConnection
) : ExceptionHandlingPresenter<SignInScene.View>(
    v,
    job,
    executor,
    humanizer,
    resourceManager
), SignInScene.Presenter {

    override fun signIn() {
        val v = view ?: return
        if (!internetConnection.isOnline) {
            view?.message(
                resourceManager.getString(R.string.error_no_inet),
                Messageable.LENGTH_SHORT,
                Messageable.SNACKBAR
            )
            return
        }
        launch(executor.ui) {
            progressManager.showProgress()
            val result = collector.collect(v)
            when (result) {
                is Result.Failure -> {
                    view?.message(
                        resourceManager.getString(result.stringRes),
                        Messageable.LENGTH_SHORT,
                        Messageable.TOAST
                    )
                }
                is Result.Success -> {
                    val data = result.data
                    OneSignal.idsAvailable { userId, registrationId ->
                        login(data.email, data.password, userId)
                    }

                }
            }.also {
                progressManager.hideProgress()
            }
        }
    }

    private fun login(email: String, password: String, userId: String) {
        launch(executor.ui) {
            val authResponse = api.loginAsync(email, password, userId).await()
            progressManager.hideProgress()
            when {
                authResponse.status == STATUS_OK -> {
                    accountManager.save(Account.Authorized(authResponse.token))
                    router.startFlow(Screen.Root.Main)
                }
                authResponse.status == STATUS_ERROR -> view?.message(
                    authResponse.message,
                    Messageable.LENGTH_SHORT,
                    Messageable.TOAST
                )
                else -> {
                }
            }
        }
    }

    override fun forgottenPassword() = router.navigateTo(Screen.Forgotten)

    override fun signUp() = router.navigateTo(Screen.SignUp)

    override val handler = ExceptionHandler { _, exception ->
        progressManager.hideProgress()
        view?.message(humanizer.humanize(exception), Messageable.LENGTH_SHORT, Messageable.TOAST)
    }
}