package app.virtualrace.scene.authorization.signin

import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.data.Result
import app.virtualrace.data.api.model.UserLogin

interface SignInScene {
    interface View : Attachable, Messageable
    interface Presenter {
        fun signIn()
        fun signUp()
        fun forgottenPassword()
    }

    interface Collector {
        fun collect(v: View): Result<UserLogin>
    }
}