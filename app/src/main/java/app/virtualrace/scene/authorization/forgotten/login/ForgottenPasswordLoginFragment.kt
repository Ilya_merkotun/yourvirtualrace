package app.virtualrace.scene.authorization.forgotten.login

import android.os.Bundle
import android.view.View
import app.virtualrace.R
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.ui.StatusBarStyleable
import app.virtualrace.core.ui.VisibleToolbar
import app.virtualrace.core.ui.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_forgotten_login.*
import toothpick.Scope
import javax.inject.Inject

class ForgottenPasswordLoginFragment : BaseFragment(), ScopeAware, ForgottenPasswordLoginScene.View,
    View.OnClickListener, VisibleToolbar, StatusBarStyleable {

    companion object {
        fun newInstance() = ForgottenPasswordLoginFragment()
    }

    override val isVisibleToolbar = true
    override val isLightStatusBar = true
    override val toolbarTitleRes = R.string.toolbar_forgotten_password
    override val layoutResId = R.layout.fragment_forgotten_login

    @Inject lateinit var presenter: ForgottenPasswordLoginScene.Presenter

    override fun onScopeCreated(scope: Scope) {
        scope.installModules(ForgottenPasswordLoginModule(this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginButton.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.loginButton -> presenter.login()
        }
    }
}