package app.virtualrace.scene.authorization.forgotten.update

import app.virtualrace.R
import app.virtualrace.account.AccountManager
import app.virtualrace.core.common.ExceptionHandler
import app.virtualrace.core.common.ExceptionHumanizer
import app.virtualrace.core.common.Executor
import app.virtualrace.core.common.ResourceManager
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.ProgressManager
import app.virtualrace.core.presentation.presenter.ExceptionHandlingPresenter
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.data.api.Api
import app.virtualrace.scene.Screen
import app.virtualrace.util.InternetConnection
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class UpdateForgottenPasswordPresenter @Inject constructor(
    v: UpdateForgottenPasswordScene.View,
    job: Job,
    executor: Executor,
    humanizer: ExceptionHumanizer,
    resourceManager: ResourceManager,
    private val router: FlowRouter,
    private val api: Api,
    private val accountManager: AccountManager,
    private val progressManager: ProgressManager,
    private val internetConnection: InternetConnection
) : ExceptionHandlingPresenter<UpdateForgottenPasswordScene.View>(
    v,
    job,
    executor,
    humanizer,
    resourceManager
), UpdateForgottenPasswordScene.Presenter {

    override fun changePassword(pin: String, password: String) {
        if (!internetConnection.isOnline) {
            view?.message(
                resourceManager.getString(R.string.error_no_inet),
                Messageable.LENGTH_SHORT,
                Messageable.SNACKBAR
            )
            return
        }
        launch(executor.ui) {
            progressManager.showProgress()
            accountManager.tempEmail?.let {
                api.forgottenUpdateAsync(it, password, pin).await()
                router.navigateTo(Screen.Login)
            }.also {
                progressManager.hideProgress()
            }
        }
    }

    override val handler = ExceptionHandler { _, exception ->
        progressManager.hideProgress()
        view?.message(humanizer.humanize(exception), Messageable.LENGTH_SHORT, Messageable.SNACKBAR)
    }
}