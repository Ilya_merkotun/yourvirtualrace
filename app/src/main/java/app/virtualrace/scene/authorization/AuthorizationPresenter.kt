package app.virtualrace.scene.authorization

import app.virtualrace.core.presentation.presenter.Presenter
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.scene.Screen
import javax.inject.Inject

class AuthorizationPresenter @Inject constructor(
    view: AuthorizationScene.View,
    private val router: FlowRouter
) : Presenter<AuthorizationScene.View>(view), AuthorizationScene.Presenter {
    override fun start() {
        router.replaceScreen(Screen.NavigationContainer(Screen.Navigation.SignIn))
    }
}