package app.virtualrace.scene.authorization.signup

import app.virtualrace.R
import app.virtualrace.data.Result
import app.virtualrace.data.api.model.UserRegister
import kotlinx.android.synthetic.main.fragment_sign_up.*

class SignUpCollector : SignUpScene.Collector {
    override fun collect(view: SignUpScene.View): Result<UserRegister> {
        if (view !is SignUpFragment) return Result.Failure(R.string.error_something_went_wrong)
        val firstName = view.firstNameEdit.text.let {
            if (it.toString().isBlank()) return Result.Failure(R.string.error_you_must_enter_first_name)
            else it.toString()
        }

        val lastName = view.lastNameEdit.text.let {
            if (it.toString().isBlank()) return Result.Failure(R.string.error_you_must_enter_last_name)
            else it.toString()
        }

        val email = view.emailEdit.text.let {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(it).matches()) it.toString()
            else return Result.Failure(R.string.error_you_must_enter_email)
        }

        val password = view.passwordEdit.text.let {
            if (it.toString().isBlank()) return Result.Failure(R.string.error_you_must_enter_password)
            else it.toString()
        }
        return Result.Success(UserRegister(email, password, firstName, lastName))
    }
}