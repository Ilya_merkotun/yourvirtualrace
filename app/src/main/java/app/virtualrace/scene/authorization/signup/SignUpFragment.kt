package app.virtualrace.scene.authorization.signup

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.CompoundButton
import androidx.core.content.ContextCompat
import app.virtualrace.R
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.ui.StatusBarStyleable
import app.virtualrace.core.ui.VisibleToolbar
import app.virtualrace.core.ui.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_sign_up.*
import toothpick.Scope
import javax.inject.Inject

class SignUpFragment : BaseFragment(), ScopeAware, SignUpScene.View,
    View.OnClickListener,
    CompoundButton.OnCheckedChangeListener,
    VisibleToolbar,
    StatusBarStyleable {

    companion object {
        fun newInstance() = SignUpFragment()
    }

    override val layoutResId get() = R.layout.fragment_sign_up
    override val toolbarTitleRes = R.string.toolbar_sign_up
    override val isLightStatusBar = true
    override val isVisibleToolbar = true

    @Inject lateinit var presenter: SignUpScene.Presenter

    override fun onScopeCreated(scope: Scope) {
        scope.installModules(SignUpModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkTerms.setOnCheckedChangeListener(this)
        signUpButton.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.grey))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.signUpButton -> presenter.signUp()
        }
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        when (buttonView.id) {
            R.id.checkTerms -> {
                signUpButton?.let {
                    it.isClickable = isChecked
                    it.isEnabled = isChecked
                    it.isFocusable = isChecked
                    if(isChecked) {
                        it.setOnClickListener(this)
                        it.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.white))
                        it.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorPrimary))
                    }
                    else {
                        it.setOnClickListener(null)
                        it.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.grey))
                        it.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.white))
                    }
                }
            }
        }
    }
}