package app.virtualrace.scene.authorization.forgotten

import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable

interface ForgottenPasswordScene {
    interface View : Attachable, Messageable
    interface Presenter {
        fun reset(email: String)
    }
}