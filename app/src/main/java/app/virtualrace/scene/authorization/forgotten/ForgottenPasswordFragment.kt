package app.virtualrace.scene.authorization.forgotten

import android.os.Bundle
import android.view.View
import app.virtualrace.R
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.ui.StatusBarStyleable
import app.virtualrace.core.ui.VisibleToolbar
import app.virtualrace.core.ui.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_forgotten_password.*
import toothpick.Scope
import javax.inject.Inject

class ForgottenPasswordFragment : BaseFragment(), ScopeAware, ForgottenPasswordScene.View,
    View.OnClickListener, VisibleToolbar, StatusBarStyleable {

    companion object {
        fun newInstance() = ForgottenPasswordFragment()
    }

    override val isVisibleToolbar = true
    override val isLightStatusBar = true
    override val toolbarTitleRes = R.string.toolbar_forgotten_password
    override val layoutResId = R.layout.fragment_forgotten_password

    @Inject lateinit var presenter: ForgottenPasswordScene.Presenter

    override fun onScopeCreated(scope: Scope) {
        scope.installModules(ForgottenPasswordModule(this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        resetPaswwordButton.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.resetPaswwordButton ->
                emailEdit.text?.let {
                    if (android.util.Patterns.EMAIL_ADDRESS.matcher(it).matches()) presenter.reset(
                        it.toString()
                    )
                    else message(
                        getString(R.string.error_you_must_enter_email),
                        Messageable.LENGTH_SHORT,
                        Messageable.TOAST
                    )
                }
        }
    }
}