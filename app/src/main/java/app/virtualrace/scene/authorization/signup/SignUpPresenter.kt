package app.virtualrace.scene.authorization.signup

import android.util.Log
import app.virtualrace.R
import app.virtualrace.account.Account
import app.virtualrace.account.AccountManager
import app.virtualrace.common.Constant
import app.virtualrace.core.common.ExceptionHandler
import app.virtualrace.core.common.ExceptionHumanizer
import app.virtualrace.core.common.Executor
import app.virtualrace.core.common.ResourceManager
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.ProgressManager
import app.virtualrace.core.presentation.presenter.ExceptionHandlingPresenter
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.data.api.Api
import app.virtualrace.scene.Screen
import app.virtualrace.util.InternetConnection
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject
import app.virtualrace.data.Result as Res

class SignUpPresenter @Inject constructor(
    v: SignUpScene.View,
    job: Job,
    executor: Executor,
    humanizer: ExceptionHumanizer,
    resourceManager: ResourceManager,
    private val router: FlowRouter,
    private val api: Api,
    private val accountManager: AccountManager,
    private val collector: SignUpScene.Collector,
    private val progressManager: ProgressManager,
    private val internetConnection: InternetConnection
) : ExceptionHandlingPresenter<SignUpScene.View>(
    v,
    job,
    executor,
    humanizer,
    resourceManager
), SignUpScene.Presenter {

    override fun signUp() {
        val v = view ?: return
        if (!internetConnection.isOnline) {
            view?.message(
                resourceManager.getString(R.string.error_no_inet),
                Messageable.LENGTH_SHORT,
                Messageable.SNACKBAR
            )
            return
        }
        launch(executor.ui) {
            progressManager.showProgress()
            val result = collector.collect(v)
            when (result) {
                is Res.Failure -> {
                    view?.message(
                        resourceManager.getString(result.stringRes),
                        Messageable.LENGTH_SHORT,
                        Messageable.TOAST
                    )
                }
                is Res.Success -> {
                    val data = result.data
                    OneSignal.idsAvailable { userId, registrationId ->
                        register(data.firstName, data.lastName, data.email, data.password, userId)
                    }
                }
            }.also {
                progressManager.hideProgress()
            }
        }
    }

    private fun register(firstName: String,
                         lastName: String,
                         email: String,
                         password: String,
                         userId: String) {
        launch(executor.ui) {
            launch(executor.ui) {

                val response = api.registerAsync(
                    firstName,
                    lastName,
                    email,
                    password,
                    userId
                ).await()
                accountManager.save(Account.Authorized(response.token))
                router.startFlow(Screen.Root.Main)
            }
        }
    }

    override val handler = ExceptionHandler { _, exception ->
        progressManager.hideProgress()
        view?.message("Problems", Messageable.LENGTH_SHORT, Messageable.SNACKBAR)
    }
}