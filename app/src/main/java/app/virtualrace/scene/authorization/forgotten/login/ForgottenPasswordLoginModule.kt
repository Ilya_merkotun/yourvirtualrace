package app.virtualrace.scene.authorization.forgotten.login

import app.virtualrace.core.di.module.CoroutineModule

class ForgottenPasswordLoginModule(f: ForgottenPasswordLoginFragment) : CoroutineModule(f) {
    init {
        bind(ForgottenPasswordLoginScene.View::class.java).toInstance(f)
        bind(ForgottenPasswordLoginScene.Presenter::class.java).to(ForgottenPasswordLoginPresenter::class.java)
            .singletonInScope()
        bind(ForgottenPasswordLoginScene.Collector::class.java).toInstance(Collector())
    }
}