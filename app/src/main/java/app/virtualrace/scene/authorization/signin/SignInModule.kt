package app.virtualrace.scene.authorization.signin

import app.virtualrace.core.di.module.CoroutineModule

class SignInModule(f: SignInFragment) : CoroutineModule(f) {
    init {
        bind(SignInScene.View::class.java).toInstance(f)
        bind(SignInScene.Presenter::class.java).to(SignInPresenter::class.java).singletonInScope()
        bind(SignInScene.Collector::class.java).toInstance(SignInCollector())
    }
}