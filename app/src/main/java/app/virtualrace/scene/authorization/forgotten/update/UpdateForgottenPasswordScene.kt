package app.virtualrace.scene.authorization.forgotten.update

import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable

interface UpdateForgottenPasswordScene {
    interface View: Attachable, Messageable
    interface Presenter {
        fun changePassword(pin: String, password: String)
    }
}