package app.virtualrace.scene.authorization.forgotten.update

import android.os.Bundle
import android.view.View
import app.virtualrace.R
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.ui.StatusBarStyleable
import app.virtualrace.core.ui.VisibleToolbar
import app.virtualrace.core.ui.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_update_forgotten_password.*
import toothpick.Scope
import javax.inject.Inject

class UpdateForgottenPasswordFragment : BaseFragment(), ScopeAware,
    UpdateForgottenPasswordScene.View, VisibleToolbar, View.OnClickListener, StatusBarStyleable {

    companion object {
        fun newInstance() = UpdateForgottenPasswordFragment()
    }

    override val toolbarTitleRes = R.string.toolbar_forgotten_password
    override val isVisibleToolbar = true
    override val layoutResId = R.layout.fragment_update_forgotten_password
    override val isLightStatusBar = true

    @Inject lateinit var presenter: UpdateForgottenPasswordScene.Presenter

    override fun onScopeCreated(scope: Scope) {
        scope.installModules(UpdateForgottenPasswordModule(this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changePasswordButton.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.changePasswordButton -> {
                when {
                    pinEdit.text.toString().isBlank() -> {
                        message(
                            getString(R.string.error_pin_code_is_empty),
                            Messageable.LENGTH_SHORT, Messageable.TOAST
                        )
                        return
                    }
                    passwordEdit.text.toString().isBlank() -> {
                        message(
                            getString(R.string.error_password_is_empty),
                            Messageable.LENGTH_SHORT, Messageable.TOAST
                        )
                        return
                    }
                    else -> presenter.changePassword(
                        pinEdit.text.toString(),
                        passwordEdit.text.toString()
                    )
                }
            }
        }
    }
}