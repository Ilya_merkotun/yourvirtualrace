package app.virtualrace.scene.authorization

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import app.virtualrace.R
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.ui.HoldingToolbarTitle
import app.virtualrace.core.ui.fragment.container.BaseContainerFragment
import app.virtualrace.scene.Screen
import kotlinx.android.synthetic.main.fragment_container_toolbar.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace
import toothpick.Scope
import javax.inject.Inject

class AuthorizationFragment : BaseContainerFragment(),
    ScopeAware,
    AuthorizationScene.View,
    HoldingToolbarTitle {
    companion object {
        fun newInstance() = AuthorizationFragment()
    }

    override val titleTextView: TextView get() = toolbarTextView
    override val layoutResId get() = R.layout.fragment_container_toolbar
    override val containerId get() = R.id.container

    @Inject
    lateinit var presenter: AuthorizationScene.Presenter

    // region ScopeAware
    override fun onScopeCreated(scope: Scope) {
        scope.installModules(
            AuthorizationModule(this, scope.parentScope.getInstance(Router::class.java))
        )
    }
    // endregion

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        supportActivity?.setSupportActionBar(toolbar)

        if (childFragmentManager.fragments.isEmpty()) {
            presenter.start()
        }
    }

    override val navigator: Navigator by lazy {
        object : SupportAppNavigator(activity, childFragmentManager, containerId) {
            override fun activityBack() = this@AuthorizationFragment.activityBack()
            override fun setupFragmentTransaction(
                command: Command,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction
            ) {
                fragmentTransaction.setReorderingAllowed(true)
                fragmentTransaction(command, currentFragment, nextFragment, fragmentTransaction)
            }

            override fun applyCommand(command: Command?) {
                val activity = activity
                if (activity == null || activity.isFinishing || activity.isDestroyed) return

                val manager = childFragmentManager

                when (command) {
                    is Replace -> {
                        val screen = command.screen as Screen
                        val target = manager.findFragmentByTag(screen.screenKey)
                            ?: createFragment(screen)

                        with(manager.beginTransaction()) {

                            val fragments = manager.fragments
                            for (fragment in fragments) {
                                if (fragment == target) continue

                                hide(fragment)
                            }

                            if (!target.isAdded) {
                                add(containerId, target, screen.screenKey)

                            } else {
                                show(target)
                            }

                            commit()
                        }
                    }
                    else -> super.applyCommand(command)
                }
            }
        }
    }
}