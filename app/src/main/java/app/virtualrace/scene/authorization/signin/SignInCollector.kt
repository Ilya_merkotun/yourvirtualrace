package app.virtualrace.scene.authorization.signin

import app.virtualrace.R
import app.virtualrace.data.Result
import app.virtualrace.data.api.model.UserLogin
import kotlinx.android.synthetic.main.fragment_sign_in.*

class SignInCollector : SignInScene.Collector {
    override fun collect(v: SignInScene.View): Result<UserLogin> {
        if (v !is SignInFragment) return Result.Failure(R.string.error_something_went_wrong)
        val email = v.emailEdit.text.let {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(it).matches()) it.toString()
            else return Result.Failure(R.string.error_you_must_enter_email)
        }

        val password = v.passWordEdit.text.let {
            if (it.toString().isBlank()) return Result.Failure(R.string.error_you_must_enter_password)
            else it.toString()
        }
        return Result.Success(UserLogin(email, password))
    }
}