package app.virtualrace.scene.authorization.forgotten

import app.virtualrace.core.di.module.CoroutineModule

class ForgottenPasswordModule(f: ForgottenPasswordFragment) : CoroutineModule(f) {
    init {
        bind(ForgottenPasswordScene.View::class.java).toInstance(f)
        bind(ForgottenPasswordScene.Presenter::class.java).to(ForgottenPasswordPresenter::class.java).singletonInScope()
    }
}