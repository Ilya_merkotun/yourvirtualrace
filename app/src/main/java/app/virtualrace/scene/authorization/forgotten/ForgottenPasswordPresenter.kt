package app.virtualrace.scene.authorization.forgotten

import app.virtualrace.R
import app.virtualrace.account.AccountManager
import app.virtualrace.core.common.ExceptionHandler
import app.virtualrace.core.common.ExceptionHumanizer
import app.virtualrace.core.common.Executor
import app.virtualrace.core.common.ResourceManager
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.ProgressManager
import app.virtualrace.core.presentation.presenter.ExceptionHandlingPresenter
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.data.api.Api
import app.virtualrace.scene.Screen
import app.virtualrace.util.InternetConnection
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class ForgottenPasswordPresenter @Inject constructor(
    v: ForgottenPasswordScene.View,
    job: Job,
    executor: Executor,
    humanizer: ExceptionHumanizer,
    resourceManager: ResourceManager,
    private val router: FlowRouter,
    private val api: Api,
    private val accountManager: AccountManager,
    private val progressManager: ProgressManager,
    private val internetConnection: InternetConnection
) : ExceptionHandlingPresenter<ForgottenPasswordScene.View>(
    v,
    job,
    executor,
    humanizer,
    resourceManager
), ForgottenPasswordScene.Presenter {

    override fun reset(email: String) {
        if (!internetConnection.isOnline) {
            view?.message(
                resourceManager.getString(R.string.error_no_inet),
                Messageable.LENGTH_SHORT,
                Messageable.SNACKBAR
            )
            return
        }
        launch(executor.ui) {
            progressManager.showProgress()
            api.forgottenAsync(email).await()
            accountManager.tempEmail = email
            progressManager.hideProgress()
            router.navigateTo(Screen.ForgottenUpdate)
        }
    }

    override val handler = ExceptionHandler { _, exception ->
        progressManager.hideProgress()
        view?.message(humanizer.humanize(exception), Messageable.LENGTH_SHORT, Messageable.TOAST)
    }
}