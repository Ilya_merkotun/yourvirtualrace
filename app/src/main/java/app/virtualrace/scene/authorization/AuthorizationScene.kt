package app.virtualrace.scene.authorization

import app.virtualrace.core.presentation.common.Starting

interface AuthorizationScene {
    interface View
    interface Presenter : Starting
}