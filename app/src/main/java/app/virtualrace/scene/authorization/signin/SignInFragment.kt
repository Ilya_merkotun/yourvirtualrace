package app.virtualrace.scene.authorization.signin

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import app.virtualrace.BuildConfig
import app.virtualrace.R
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.ui.StatusBarStyleable
import app.virtualrace.core.ui.VisibleToolbar
import app.virtualrace.core.ui.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_sign_in.*
import toothpick.Scope
import javax.inject.Inject

class SignInFragment : BaseFragment(), ScopeAware, SignInScene.View, StatusBarStyleable,
    View.OnClickListener, VisibleToolbar {

    companion object {
        fun newInstance() = SignInFragment()
    }

    override val layoutResId get() = R.layout.fragment_sign_in
    override val isLightStatusBar = true
    override val isVisibleToolbar: Boolean
        get() = false

    @Inject lateinit var presenter: SignInScene.Presenter

    override fun onScopeCreated(scope: Scope) {
        scope.installModules(SignInModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signUpButton.setOnClickListener(this)
        loginButton.setOnClickListener(this)
        forgottenText.setOnClickListener(this)

        if (BuildConfig.DEBUG) {
            emailEdit.setTextKeepState("brian@digitalabu.com")
            passWordEdit.setTextKeepState("d8f7\$drj")
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.loginButton -> presenter.signIn()
            R.id.signUpButton -> presenter.signUp()
            R.id.forgottenText -> presenter.forgottenPassword()
        }
    }
}