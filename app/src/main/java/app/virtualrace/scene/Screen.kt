package app.virtualrace.scene

import app.virtualrace.core.ui.fragment.DummyFragment
import app.virtualrace.scene.authorization.AuthorizationFragment
import app.virtualrace.scene.authorization.forgotten.ForgottenPasswordFragment
import app.virtualrace.scene.authorization.forgotten.login.ForgottenPasswordLoginFragment
import app.virtualrace.scene.authorization.forgotten.update.UpdateForgottenPasswordFragment
import app.virtualrace.scene.authorization.signin.SignInFragment
import app.virtualrace.scene.authorization.signup.SignUpFragment
import app.virtualrace.scene.challenge.complete.CompleteFragment
import app.virtualrace.scene.challenge.prepare.PrepareFragment
import app.virtualrace.scene.challenge.result.ResultFragment
import app.virtualrace.scene.challenge.run.RunFragment
import app.virtualrace.scene.challenge.single.ChallengeSingleFragment
import app.virtualrace.scene.challenge.team.TeamsFragment
import app.virtualrace.scene.main.MainFragment
import app.virtualrace.scene.main.challenges.ChallengesListFragment
import app.virtualrace.scene.main.challenges.my.MyChallengesListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import java.io.Serializable

sealed class Screen : SupportAppScreen() {
    sealed class Root : Screen() {
        object Main : Root() {
            override fun getFragment() = MainFragment.newInstance()
        }

        object Authorization : Root() {
            override fun getFragment() = AuthorizationFragment.newInstance()
        }
    }

    class NavigationContainer(private val root: Navigation) : Screen() {
        override fun getFragment() = NavigationContainerFragment.newInstance(root)
        override fun getScreenKey(): String = "Navigation@${root.screenKey}"
    }

    sealed class Navigation : Screen(), Serializable {
        object SignIn : Navigation() {
            override fun getFragment() = SignInFragment.newInstance()
        }

        sealed class Main : Navigation() {
            object AllChallenges : Main() {
                override fun getFragment() = ChallengesListFragment.newInstance()
            }

            object MyChallenges : Main() {
                override fun getFragment() = MyChallengesListFragment.newInstance()
            }

            object MyRuns : Main() {
                override fun getFragment() = DummyFragment.newInstance("Joined Runs")
            }
        }
    }

    class DummyScreen(private val text: String? = null) : Screen() {
        override fun getFragment() = DummyFragment.newInstance(text)
    }

    // region Authorization
    object SignUp : Screen() {
        override fun getFragment() = SignUpFragment.newInstance()
    }

    object ForgottenUpdate : Screen() {
        override fun getFragment() = UpdateForgottenPasswordFragment.newInstance()
    }

    object Forgotten : Screen() {
        override fun getFragment() = ForgottenPasswordFragment.newInstance()
    }

    object Login : Screen() {
        override fun getFragment() = ForgottenPasswordLoginFragment.newInstance()
    }
    // endregion

    //region Main
    sealed class Challenge : Screen() {
        object Single : Challenge() {
            override fun getFragment() = ChallengeSingleFragment.newInstance()
        }

        object Prepare : Challenge() {
            override fun getFragment() = PrepareFragment.newInstance()
        }

        object Run : Challenge() {
            override fun getFragment() = RunFragment.newInstance()
        }

        object Complete : Challenge() {
            override fun getFragment() = CompleteFragment.newInstance()
        }

        object Team : Challenge() {
            override fun getFragment() = TeamsFragment.newInstance()
        }

        class Result(private val id: Int) : Challenge() {
            override fun getFragment() = ResultFragment.newInstance(id)
        }

    }
    //endregion
}