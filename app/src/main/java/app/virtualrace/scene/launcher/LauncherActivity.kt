package app.virtualrace.scene.launcher

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.core.app.ActivityCompat
import app.virtualrace.R
import app.virtualrace.common.internal.activity.ActivityResultManager
import app.virtualrace.core.ui.activity.NavigatorActivity
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import toothpick.Scope
import javax.inject.Inject

class LauncherActivity : NavigatorActivity(), LauncherScene.View {

    @Inject lateinit var presenter: LauncherScene.Presenter
    @Inject lateinit var activityResultManager: ActivityResultManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.activity_launcher)

        if (supportFragmentManager.fragments.isEmpty()) presenter.launch()
    }

    override fun onScopeCreated(scope: Scope) {
        super.onScopeCreated(scope)
        scope.installModules(LauncherModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

    }
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        val delegate = ActivityCompat.getPermissionCompatDelegate()
        if (delegate is ActivityCompat.OnRequestPermissionsResultCallback) {
            delegate.onRequestPermissionsResult(requestCode, permissions, grantResults)
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!activityResultManager.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override val navigator =
        object : SupportAppNavigator(this, supportFragmentManager, R.id.container) {}
}