package app.virtualrace.scene.launcher

import app.virtualrace.account.Account
import app.virtualrace.account.AccountManager
import app.virtualrace.core.presentation.presenter.Presenter
import app.virtualrace.scene.Screen
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class LauncherPresenter @Inject constructor(
    view: LauncherScene.View,
    private val router: Router,
    private val manager: AccountManager
) : Presenter<LauncherScene.View>(view), LauncherScene.Presenter {
    override fun launch() = router.replaceScreen(
        if (manager.account is Account.Authorized) Screen.Root.Main else Screen.Root.Authorization
    )
}