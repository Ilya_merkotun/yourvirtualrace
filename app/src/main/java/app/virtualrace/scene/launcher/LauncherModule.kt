package app.virtualrace.scene.launcher

import android.app.Activity
import androidx.fragment.app.FragmentActivity
import app.virtualrace.core.extension.androidJob
import app.virtualrace.core.presentation.ProgressManager
import app.virtualrace.core.presentation.internal.DialogProgressManager
import kotlinx.coroutines.Job
import toothpick.config.Module

class LauncherModule(activity: LauncherActivity) : Module() {
    init {
        bind(Job::class.java).toInstance(androidJob(activity))
        bind(Activity::class.java).toInstance(activity)
        bind(FragmentActivity::class.java).toInstance(activity)
        bind(ProgressManager::class.java).to(DialogProgressManager::class.java).singletonInScope()

        bind(LauncherScene.View::class.java).toInstance(activity)
        bind(LauncherScene.Presenter::class.java).to(LauncherPresenter::class.java)
            .singletonInScope()
    }
}