package app.virtualrace.scene.launcher

import app.virtualrace.core.presentation.common.Launching

interface LauncherScene {
    interface View
    interface Presenter : Launching
}