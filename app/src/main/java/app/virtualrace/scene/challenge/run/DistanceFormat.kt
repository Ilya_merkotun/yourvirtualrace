package app.virtualrace.scene.challenge.run

import app.virtualrace.util.ConvertDistance

sealed class DistanceFormat {
    class M(val distance: Int) : DistanceFormat() {
        override fun toString() = "$distance${ConvertDistance.M}"
    }

    class KM(val distance: Double) : DistanceFormat() {
        override fun toString() = "$distance${ConvertDistance.KM}"
    }

    class MILES(val distance: Double) : DistanceFormat() {
        override fun toString() = "$distance${ConvertDistance.MILES}"
    }
}