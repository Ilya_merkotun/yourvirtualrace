package app.virtualrace.scene.challenge.result

import app.virtualrace.core.common.ExceptionHandler
import app.virtualrace.core.common.ExceptionHumanizer
import app.virtualrace.core.common.Executor
import app.virtualrace.core.common.ResourceManager
import app.virtualrace.core.location.Location
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.ProgressManager
import app.virtualrace.core.presentation.presenter.ExceptionHandlingPresenter
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.data.Wrapper
import app.virtualrace.data.api.Api
import app.virtualrace.scene.challenge.run.DistanceFormat
import app.virtualrace.scene.main.provider.ChallengeData
import app.virtualrace.scene.main.provider.ChallengesStore
import app.virtualrace.util.ConvertDistance
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class ResultPresenter @Inject constructor(
    v: ResultScene.View,
    job: Job,
    executor: Executor,
    humanizer: ExceptionHumanizer,
    resourceManager: ResourceManager,
    private val idWrap: Wrapper.Int,
    private val api: Api,
    private val flowRouter: FlowRouter,
    private val progressManager: ProgressManager,
    private val challengesStore: ChallengesStore
) : ExceptionHandlingPresenter<ResultScene.View>(
    v,
    job,
    executor,
    humanizer,
    resourceManager
), ResultScene.Presenter {

    override fun start() {
        launch(executor.ui) {
            val data = challengesStore.challengeById(idWrap.value)
            when (data) {
                is ChallengeData.Completed -> {

                    val distanceFormat = when {
                        data.data.challenge.distanceType == ConvertDistance.KM && data.data.distance!=null && data.data.distance >= 1000
                        -> DistanceFormat.KM(ConvertDistance.round(data.data.distance / 1000.0))

                        data.data.challenge.distanceType == ConvertDistance.KM && data.data.distance!=null && data.data.distance < 1000
                        -> DistanceFormat.M(data.data.distance.toInt())

                        else -> DistanceFormat.MILES(
                            ConvertDistance.changeFormatDistance(data.data.distance?.div(1000.0) ?: 0.0, true)
                        )
                    }

                    data.data.let {
                        view?.showInfo(
                            distanceFormat.toString(),
                            it.averagePace,
                            it.time
                        )
                    }
                }
            }
        }
    }

    override fun updateMap() {
        launch(executor.ui) {

            val data = challengesStore.challengeById(idWrap.value)
            when (data) {
                is ChallengeData.Completed -> {
                    data.data.let {
                        view?.showPoints(data.data.points.map { Location(it.lat, it.lon) })
                    }
                }
            }
        }
    }

    override val handler = ExceptionHandler { _, exception ->
        progressManager.hideProgress()
        view?.message(humanizer.humanize(exception), Messageable.LENGTH_SHORT, Messageable.TOAST)
    }
}