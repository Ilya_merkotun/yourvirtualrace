package app.virtualrace.scene.challenge.prepare

import android.Manifest
import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.common.Starting

interface PrepareScene {
    interface View : Attachable, Messageable {
        fun showTime(sec: Int, isEnabled: Boolean, title: String?)
    }

    interface Presenter : Starting {
        fun run()
    }

    companion object {
        val PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }
}