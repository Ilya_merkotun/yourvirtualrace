package app.virtualrace.scene.challenge.run

import app.virtualrace.R
import app.virtualrace.core.common.*
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.ProgressManager
import app.virtualrace.core.presentation.presenter.ExceptionHandlingPresenter
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.data.api.Api
import app.virtualrace.data.api.model.MyChallenge
import app.virtualrace.extension.distanceTo
import app.virtualrace.scene.Screen
import app.virtualrace.scene.main.provider.ChallengeData
import app.virtualrace.scene.main.provider.ChallengesStore
import app.virtualrace.scene.main.provider.RunningData
import app.virtualrace.util.ConvertDistance
import app.virtualrace.util.LocationServiceDataStorage
import app.virtualrace.util.LocationServiceListener
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.math.roundToInt
import app.virtualrace.core.location.Location as CoreLocation

class RunPresenter @Inject constructor(
    v: RunScene.View,
    job: Job,
    executor: Executor,
    humanizer: ExceptionHumanizer,
    resourceManager: ResourceManager,
    private val api: Api,
    private val flowRouter: FlowRouter,
    private val progressManager: ProgressManager,
    private val challengesStore: ChallengesStore,
    private val permissionManager: PermissionManager,
    private val dataStorage: LocationServiceDataStorage,
    private val runningData: RunningData
) : ExceptionHandlingPresenter<RunScene.View>(
    v,
    job,
    executor,
    humanizer,
    resourceManager
), RunScene.Presenter,
    PermissionsCallback,
    LocationServiceListener
{

    private lateinit var challenge: MyChallenge

    private var currentDistance: Double = 0.0  //meters always
    private var currentTimeSec: Int = 0  //sec always
    private var lastSavedTimeMillis: Long? = null
    private var lastSavedLocation: CoreLocation? = null

    private var arrayPoints = arrayListOf<CoreLocation>()
    private var isRunState: Boolean = false
        set(value) {
            if (value) {
                lastSavedLocation = null
                lastSavedTimeMillis = null
            }
            field = value
        }

    private val isChallengeDone
        get() =
            if (challenge.challenge.distanceType == ConvertDistance.KM)
                currentDistance / 1000.0 >= challenge.challenge.distance
            else ConvertDistance.changeFormatDistance(
                currentDistance / 1000.0,
                true
            ) >= challenge.challenge.distance

    override fun start() {
        runningData.clearRun()
        val time = 1 // 1 sec
        launch(executor.io) {
            while (true) {
                while (isRunState) {
                    delay(time * 1000L)
                    currentTimeSec += time
                    withContext(executor.ui) {
                        view?.showTime(currentTimeSec)
                    }
                }
            }
        }

        launch(executor.ui) {
            challenge = (challengesStore.challengeById(runningData.id) as ChallengeData.Joined).data
            view?.showTitle(challenge.challenge.name)

            if (!permissionManager.granted)
                permissionManager.request(this@RunPresenter, *RunScene.PERMISSIONS)
            else {
                isRunState = true
                run()
            }
        }
    }

    //region RunScene.Presenter
    override fun action() {
        isRunState = isRunState.not()
        run()
    }

    private fun stop() {
        isRunState = false
        run()
        launch(executor.ui) {

            runningData.type = challenge.challenge.distanceType
            runningData.currentDistance = currentDistance.toInt()
            runningData.currentTime = currentTimeSec
            runningData.points = arrayPoints

            flowRouter.replaceScreen(Screen.Challenge.Complete)
        }
    }
    //endregion

    override fun onPermissionsGranted(granted: Boolean) {
        if (granted) {
            isRunState = true
            run()
        } else {
            view?.message(
                resourceManager.getString(R.string.error_no_permission),
                Messageable.LENGTH_SHORT,
                Messageable.TOAST
            )
        }
    }

    private fun run() {
        view?.showState(isRunState)
        if (isRunState) {
            dataStorage.setListener(this)
        } else {
            dataStorage.removeListener()
        }
    }

    private fun calculate(location: CoreLocation) {
        currentDistance += lastSavedLocation?.distanceTo(location)?.roundToInt() ?: 0
        lastSavedLocation = location
        arrayPoints.add(location)

        val distanceFormat = when {
            challenge.challenge.distanceType == ConvertDistance.KM && currentDistance >= 1000
            -> DistanceFormat.KM(ConvertDistance.round(currentDistance.toDouble() / 1000.0))

            challenge.challenge.distanceType == ConvertDistance.KM && currentDistance < 1000
            -> DistanceFormat.M(currentDistance.toInt())

            else -> DistanceFormat.MILES(
                ConvertDistance.changeFormatDistance(currentDistance / 1000.0, true)
            )
        }

        if (currentDistance != 0.0) {
            view?.showCurrentValues(
                distanceFormat,
                challenge.challenge.distance,
                challenge.challenge.distanceType,
                currentTimeSec / (currentDistance / 1000)
            )
        } else {
            view?.showCurrentValues(
                distanceFormat,
                challenge.challenge.distance,
                challenge.challenge.distanceType,
                0.0
            )
        }

        if (isChallengeDone) {
            stop()
        }
    }

    override fun onLocationUpdate(location: CoreLocation?) {
        location ?: return
        if (location.speed > 10) {
            isRunState = false
            run()
            view?.extraStop()
        } else calculate(location)
    }

    override val handler = ExceptionHandler { _, exception ->
        progressManager.hideProgress()
        view?.message(humanizer.humanize(exception), Messageable.LENGTH_SHORT, Messageable.TOAST)
    }
}