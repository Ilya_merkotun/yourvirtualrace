package app.virtualrace.scene.challenge.complete

import app.virtualrace.common.Constant
import app.virtualrace.core.common.ExceptionHandler
import app.virtualrace.core.common.ExceptionHumanizer
import app.virtualrace.core.common.Executor
import app.virtualrace.core.common.ResourceManager
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.ProgressManager
import app.virtualrace.core.presentation.presenter.ExceptionHandlingPresenter
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.data.api.Api
import app.virtualrace.data.api.model.CompleteBody
import app.virtualrace.scene.Screen
import app.virtualrace.scene.challenge.run.DistanceFormat
import app.virtualrace.scene.main.provider.ChallengesStore
import app.virtualrace.scene.main.provider.RunningData
import app.virtualrace.util.ConvertDistance
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class CompletePresenter @Inject constructor(
    v: CompleteScene.View,
    job: Job,
    executor: Executor,
    humanizer: ExceptionHumanizer,
    resourceManager: ResourceManager,
    private val data: RunningData,
    private val api: Api,
    private val flowRouter: FlowRouter,
    private val progressManager: ProgressManager,
    private val challengesStore: ChallengesStore
) : ExceptionHandlingPresenter<CompleteScene.View>(
    v,
    job,
    executor,
    humanizer,
    resourceManager
), CompleteScene.Presenter {

    override fun start() {
        val distanceFormat = when {
            data.type == ConvertDistance.KM && data.currentDistance >= 1000
            -> DistanceFormat.KM(ConvertDistance.round((data.currentDistance / 1000.0).toDouble()))

            data.type == ConvertDistance.KM && data.currentDistance < 1000
            -> DistanceFormat.M(data.currentDistance.toInt())

            else -> DistanceFormat.MILES(
                ConvertDistance.changeFormatDistance((data.currentDistance / 1000.0).toDouble(), true)
            )
        }

        view?.showInfo(
            distanceFormat.toString(),
            data.averagePace,
            data.currentTime
        )
    }

    override fun complete() {
        launch(executor.ui) {
            val status =  api.completeAsync(
                data.id,
                CompleteBody(
                    data.currentDistance.toFloat(),
                    data.team?.id ,
                    data.currentTime,
                    data.averagePace.toFloat(),
                    data.points.map { "${it.latitude},${it.longitude}" }
                )
            ).await()
            if(status.status == Constant.STATUS_SUCCESS) {
                flowRouter.backTo(Screen.Challenge.Single)
            }
        }
    }

    override fun updateMap() {
        view?.showPoints(data.points)
    }

    override val handler = ExceptionHandler { _, exception ->
        progressManager.hideProgress()
        view?.message(humanizer.humanize(exception), Messageable.LENGTH_SHORT, Messageable.TOAST)
    }
}