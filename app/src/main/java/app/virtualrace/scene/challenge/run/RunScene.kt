package app.virtualrace.scene.challenge.run

import android.Manifest
import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.common.Starting

interface RunScene {
    interface View : Attachable, Messageable {
        fun showTitle(title: String)
        fun showCurrentValues(
            distanceFormat: DistanceFormat,
            totalDistance: Double,
            totalType: String,
            averagePace: Double
        )
        fun showTime(sec: Int)
        fun showState(isRun: Boolean)
        fun extraStop()
    }

    interface Presenter : Starting {
        fun action()
    }

    companion object {
        val PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }
}