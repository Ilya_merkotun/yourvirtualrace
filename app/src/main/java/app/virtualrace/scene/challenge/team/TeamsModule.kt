package app.virtualrace.scene.challenge.team

import app.virtualrace.core.di.module.CoroutineModule

class TeamsModule(f: TeamsFragment) : CoroutineModule(f) {
    init {
        bind(TeamsScene.View::class.java).toInstance(f)
        bind(TeamsScene.Presenter::class.java).to(TeamsPresenter::class.java).singletonInScope()
    }
}