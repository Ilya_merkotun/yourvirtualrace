package app.virtualrace.scene.challenge.team

import app.virtualrace.core.common.ExceptionHandler
import app.virtualrace.core.common.ExceptionHumanizer
import app.virtualrace.core.common.Executor
import app.virtualrace.core.common.ResourceManager
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.ProgressManager
import app.virtualrace.core.presentation.StateView
import app.virtualrace.core.presentation.presenter.ExceptionHandlingPresenter
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.data.api.model.Team
import app.virtualrace.scene.Screen
import app.virtualrace.scene.main.provider.ChallengeData
import app.virtualrace.scene.main.provider.ChallengesStore
import app.virtualrace.scene.main.provider.RunningData
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class TeamsPresenter @Inject constructor(
    v: TeamsScene.View,
    job: Job,
    executor: Executor,
    humanizer: ExceptionHumanizer,
    resourceManager: ResourceManager,
    private val flowRouter: FlowRouter,
    private val progressManager: ProgressManager,
    private val runningData: RunningData,
    private val challengesStore: ChallengesStore
) : ExceptionHandlingPresenter<TeamsScene.View>(
    v,
    job,
    executor,
    humanizer,
    resourceManager
), TeamsScene.Presenter {

    private val teams = mutableListOf<Team>()

    override fun start() {
        launch(executor.ui) {
            view?.showState(StateView.State.Progress)

            when (val data = challengesStore.challengeById(runningData.id)) {
                is ChallengeData.Completed -> {
                    view?.showState(StateView.State.Empty)
                }
                is ChallengeData.Joined -> {
                    teams.clear()
                    teams.addAll(data.data.challenge.teams)
                    view?.showTeams(teams)
                    view?.setTitle(data?.data.challenge.teamType)
                    view?.showState(StateView.State.Content)
                }
                is ChallengeData.All -> view?.showState(StateView.State.Empty)
            }
        }
    }

    override fun navigateTo(position: Int) {
        runningData.team = teams[position]
        flowRouter.navigateTo(Screen.Challenge.Prepare)
    }

    override val handler = ExceptionHandler { _, exception ->
        progressManager.hideProgress()
        view?.message(humanizer.humanize(exception), Messageable.LENGTH_SHORT, Messageable.TOAST)
    }
}