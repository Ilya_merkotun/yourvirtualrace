package app.virtualrace.scene.challenge.single

import app.virtualrace.core.di.module.CoroutineModule

class ChallengeSingleModule(f: ChallengeSingleFragment) :
    CoroutineModule(f) {
    init {
        bind(ChallengeSingleScene.View::class.java).toInstance(f)
        bind(ChallengeSingleScene.Presenter::class.java).to(ChallengeSinglePresenter::class.java)
            .singletonInScope()
    }
}