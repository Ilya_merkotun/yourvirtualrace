package app.virtualrace.scene.challenge.team

import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.StateView
import app.virtualrace.core.presentation.common.Starting
import app.virtualrace.data.api.model.Team

interface TeamsScene {
    interface View: Attachable, Messageable, StateView {
        fun showTeams(list: List<Team>)
        fun setTitle(title: String?)
    }
    interface Presenter : Starting {
        fun navigateTo(position: Int)
    }
}