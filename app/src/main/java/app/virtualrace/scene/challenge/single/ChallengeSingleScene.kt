package app.virtualrace.scene.challenge.single

import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.common.Starting
import app.virtualrace.scene.main.provider.ChallengeType

interface ChallengeSingleScene {
    interface View : Attachable, Messageable {
        fun showInfo(
            name: String,
            distance: Double,
            typeDistance: String?,
            currentParti: Int,
            alreadyRun: Int,
            description: String?,
            image: String?,
            startDate: String?,
            endDate: String,
            type: ChallengeType
        )

        fun showMoreInfo(isShow:Boolean)
        fun openUrl(url: String)
    }

    interface Presenter : Starting {
        fun moreInfo()
        fun action()
    }
}