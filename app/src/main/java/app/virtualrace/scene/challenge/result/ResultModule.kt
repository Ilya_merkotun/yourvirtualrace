package app.virtualrace.scene.challenge.result

import app.virtualrace.core.di.module.CoroutineModule
import app.virtualrace.data.Wrapper
import app.virtualrace.data.wrap

class ResultModule(f: ResultFragment, id: Int) : CoroutineModule(f) {
    init {
        bind(ResultScene.View::class.java).toInstance(f)
        bind(Wrapper.Int::class.java).toInstance(id.wrap())
        bind(ResultScene.Presenter::class.java).to(ResultPresenter::class.java).singletonInScope()
    }
}