package app.virtualrace.scene.challenge.complete

import app.virtualrace.core.di.module.CoroutineModule

class CompleteModule(f: CompleteFragment) : CoroutineModule(f) {
    init {
        bind(CompleteScene.View::class.java).toInstance(f)
        bind(CompleteScene.Presenter::class.java).to(CompletePresenter::class.java).singletonInScope()
    }
}