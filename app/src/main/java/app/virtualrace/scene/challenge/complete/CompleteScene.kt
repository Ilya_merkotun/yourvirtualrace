package app.virtualrace.scene.challenge.complete

import app.virtualrace.core.location.Location
import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.common.Starting

interface CompleteScene  {
    interface View: Attachable, Messageable {
        fun showInfo(
            distance: String,
            pace: Double,
            time: Int //TODO: add points to map
        )

        fun showPoints(list: List<Location>)
    }
    interface Presenter: Starting {
        fun complete()
        fun updateMap()
    }
}