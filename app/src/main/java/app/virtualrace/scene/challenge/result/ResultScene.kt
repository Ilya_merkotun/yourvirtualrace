package app.virtualrace.scene.challenge.result

import app.virtualrace.core.location.Location
import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.common.Starting

interface ResultScene  {
    interface View: Attachable, Messageable {
        fun showInfo(
            distance: String?,
            pace: Double?,
            time: Int?
        )

        fun showPoints(list: List<Location>)
    }
    interface Presenter: Starting {
        fun updateMap()
    }
}