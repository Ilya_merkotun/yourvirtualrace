package app.virtualrace.scene.challenge.complete

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import app.virtualrace.R
import app.virtualrace.core.common.OnBackPressedHelper
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.location.Location
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.core.ui.StatusBarStyleable
import app.virtualrace.core.ui.VisibleBottomBar
import app.virtualrace.core.ui.VisibleToolbar
import app.virtualrace.core.ui.fragment.BaseFragment
import app.virtualrace.scene.Screen
import app.virtualrace.scene.challenge.result.ResultFragment
import app.virtualrace.util.TimeFormat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import kotlinx.android.synthetic.main.fragment_result.*
import toothpick.Scope
import java.math.BigDecimal
import java.math.RoundingMode
import javax.inject.Inject

class CompleteFragment : BaseFragment(),
    CompleteScene.View,
    VisibleToolbar,
    VisibleBottomBar,
    StatusBarStyleable,
    ScopeAware,
    FragmentManager.OnBackStackChangedListener,
    OnMapReadyCallback,
    View.OnClickListener {

    companion object {
        const val INITIAL_ZOOM = 17f
        fun newInstance() = CompleteFragment()
    }

    private lateinit var map: GoogleMap

    override val toolbarTitleRes = R.string.toolbar_congratulations
    override val layoutResId = R.layout.fragment_result
    override val isVisibleBottomBar = false
    override val isVisibleToolbar = true
    override val isLightStatusBar = true

    @Inject lateinit var presenter: CompleteScene.Presenter
    @Inject lateinit var router: FlowRouter
    @Inject lateinit var helper: OnBackPressedHelper

    override fun onScopeCreated(scope: Scope) =
        scope.installModules(CompleteModule(this))

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = super.onCreateView(inflater, container, savedInstanceState).also {
        (childFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment?)?.let {
            it.getMapAsync(this)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        completeButton.setOnClickListener(this)
        presenter.start()
    }

    override fun onMapReady(m: GoogleMap) {
        map = m
        map.uiSettings.setAllGesturesEnabled(true)
        map.setOnMapLoadedCallback {
            presenter.updateMap()
        }
    }

    override fun showInfo(distance: String, pace: Double, time: Int) {
        distanceText.text = distance
        paceText.text = "${TimeFormat.formatFromSec( BigDecimal(pace).setScale(2, RoundingMode.HALF_EVEN).toInt())}/km "
        timeText.text = "$time sec"
    }

    override fun showPoints(list: List<Location>) {
        if(list.isEmpty()) return

        val bound = LatLngBounds.Builder()
        val polylineOptions = PolylineOptions()
            .color(ContextCompat.getColor(requireContext(), R.color.map))
            .width(4f)
        list.forEach{
            bound.include(LatLng(it.latitude, it.longitude))
            polylineOptions.add(LatLng(it.latitude, it.longitude))
        }

        map.animateCamera(CameraUpdateFactory.newLatLngBounds( bound.build(), 75))

        map.addMarker(MarkerOptions().position(LatLng(list.first().latitude, list.first().longitude)).title("Start"))
        map.addMarker(MarkerOptions().position(LatLng(list.last().latitude, list.last().longitude)).title("End"))

        map.addPolyline(polylineOptions)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.completeButton -> presenter.complete()
        }
    }

    protected val FragmentManager.isBackStackEmpty: Boolean get() = backStackEntryCount == 0
    // region FragmentManager.OnBackStackChangedListener
    override fun onBackStackChanged() {
        val bar = supportActivity?.supportActionBar ?: return
        val isBackAllowed = !childFragmentManager.isBackStackEmpty

        bar.setHomeButtonEnabled(isBackAllowed)
        bar.setDisplayHomeAsUpEnabled(isBackAllowed)

        callback.isEnabled = true
    }
    // endregion

    override fun didBecomeActive() {
        onBackStackChanged()

        supportActivity?.let {
            it.onBackPressedDispatcher.addCallback(this, callback)
            childFragmentManager.addOnBackStackChangedListener(this)
        }
    }

    override fun didResignActive() {
        childFragmentManager.removeOnBackStackChangedListener(this)

        callback.isEnabled = false
        callback.remove()
    }

    private val callback = object : OnBackPressedCallback(false) {
        override fun handleOnBackPressed() {
            val activity = activity ?: return
            val isBackStackEmpty = fragmentManager?.isBackStackEmpty ?:false
            val skip = activity.isDestroyed || activity.isFinishing

            if (isHidden || skip || (isBackStackEmpty && helper.invokedFromToolbar)) return

            if (isBackStackEmpty) router.finishFlow() else router.backTo(Screen.Challenge.Single)
        }
    }
}