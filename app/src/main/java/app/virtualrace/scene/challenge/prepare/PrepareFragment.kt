package app.virtualrace.scene.challenge.prepare

import android.os.Bundle
import android.view.View
import app.virtualrace.BuildConfig
import app.virtualrace.R
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.ui.HoldingToolbarTitle
import app.virtualrace.core.ui.StatusBarStyleable
import app.virtualrace.core.ui.VisibleBottomBar
import app.virtualrace.core.ui.VisibleToolbar
import app.virtualrace.core.ui.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_prepare.*
import toothpick.Scope
import javax.inject.Inject

class PrepareFragment : BaseFragment(),
    VisibleToolbar,
    VisibleBottomBar,
    StatusBarStyleable,
    ScopeAware,
    View.OnClickListener,
    PrepareScene.View {

    companion object {
        fun newInstance() = PrepareFragment()
    }

    override var toolbarTitle: CharSequence?
        get() =
            (parentFragment as? HoldingToolbarTitle)?.titleTextView?.text
        set(value) {
            (parentFragment as? HoldingToolbarTitle)?.titleTextView?.text = value
        }

    override val layoutResId = R.layout.fragment_prepare
    override val isVisibleBottomBar = false
    override val isVisibleToolbar = true
    override val isLightStatusBar = true

    @Inject lateinit var presenter: PrepareScene.Presenter

    override fun onScopeCreated(scope: Scope) {
        scope.installModules(PrepareModule(this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.start()

        timerText.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.timerText -> presenter.run()
        }
    }

    override fun showTime(sec: Int, isEnabled: Boolean, title: String?) {
        timerText.isEnabled = isEnabled
        timerText.text = "$sec"
        toolbarTitle = title
    }
}