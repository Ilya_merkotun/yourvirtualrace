package app.virtualrace.scene.challenge.run

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentManager
import app.virtualrace.R
import app.virtualrace.core.common.OnBackPressedHelper
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.core.ui.HoldingToolbarTitle
import app.virtualrace.core.ui.StatusBarStyleable
import app.virtualrace.core.ui.VisibleBottomBar
import app.virtualrace.core.ui.VisibleToolbar
import app.virtualrace.core.ui.fragment.BaseFragment
import app.virtualrace.scene.Screen
import app.virtualrace.util.LocationTrackingService
import app.virtualrace.util.TimeFormat
import kotlinx.android.synthetic.main.fragment_run.*
import toothpick.Scope
import java.math.BigDecimal
import java.math.RoundingMode
import javax.inject.Inject

class RunFragment : BaseFragment(),
    VisibleToolbar,
    VisibleBottomBar,
    StatusBarStyleable,
    ScopeAware,
    View.OnClickListener,
    FragmentManager.OnBackStackChangedListener,
    RunScene.View {

    companion object {
        fun newInstance() = RunFragment()
    }

    override var toolbarTitle: CharSequence?
        get() =
            (parentFragment as? HoldingToolbarTitle)?.titleTextView?.text
        set(value) {
            (parentFragment as? HoldingToolbarTitle)?.titleTextView?.text = value
        }

    override val layoutResId = R.layout.fragment_run
    override val isVisibleBottomBar = false
    override val isVisibleToolbar = true
    override val isLightStatusBar = true
    override val isBackAllowed = false

    @Inject lateinit var presenter: RunScene.Presenter
    @Inject lateinit var router: FlowRouter
    @Inject lateinit var helper: OnBackPressedHelper

    override fun onScopeCreated(scope: Scope) {
        scope.installModules(RunModule(this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionButton.setOnClickListener(this)
        presenter.start()
    }

    //region RunScene.View

    override fun showTitle(title: String) {
        toolbarTitle = title
    }

    override fun showCurrentValues(
        distanceFormat: DistanceFormat,
        totalDistance: Double,
        totalType: String,
        averagePace: Double
    ) {
        distanceText?.text = "${distanceFormat} / $totalDistance$totalType"
        averagePaceText?.text = "${TimeFormat.formatFromSec( BigDecimal(averagePace).setScale(2, RoundingMode.HALF_EVEN).toInt())}/km "
    }

    override fun showTime(sec: Int) {
        timeText?.text = TimeFormat.formatFromSec(sec)
    }

    override fun showState(isRun: Boolean) {
        if (isRun) {
            actionButton?.let {
                it.setBackgroundResource(R.drawable.shape_pause)
                it.setImageResource(R.drawable.vec_pause)
            }
        } else {
            actionButton?.let {
                it.setBackgroundResource(R.drawable.shape_play)
                it.setImageResource(R.drawable.vec_play)
            }
        }
    }
    //endregion

    override fun onClick(v: View) {
        when (v.id) {
            R.id.actionButton -> presenter.action()
        }
    }

    // region FragmentManager.OnBackStackChangedListener
    override fun onBackStackChanged() {
        val bar = supportActivity?.supportActionBar ?: return
        var isBackAllowed = !childFragmentManager.isBackStackEmpty

        isBackAllowed = false  //solution for client

        bar.setHomeButtonEnabled(isBackAllowed)
        bar.setDisplayHomeAsUpEnabled(isBackAllowed)

        callback.isEnabled = true
    }
    // endregion

    override fun onResume() {
        super.onResume()
        didBecomeActive()//hotfix
    }

    override fun didBecomeActive() {
        onBackStackChanged()

        supportActivity?.let {
            it.onBackPressedDispatcher.addCallback(this, callback)
            childFragmentManager.addOnBackStackChangedListener(this)
        }
    }

    private val FragmentManager.isBackStackEmpty: Boolean get() = backStackEntryCount == 0

    override fun didResignActive() {
        childFragmentManager.removeOnBackStackChangedListener(this)

        callback.isEnabled = false
        callback.remove()
    }

    override fun onDestroy() {
        LocationTrackingService.stop(requireActivity())
        super.onDestroy()
    }

    override fun extraStop() {
        AlertDialog.Builder(requireContext()).apply {
            setTitle("You're running too fast for the app, sorry.")
            setPositiveButton("Ok" ){ d,_ ->
                d.dismiss()
                val isBackStackEmpty = fragmentManager?.isBackStackEmpty ?: false
                if (isBackStackEmpty) router.finishFlow() else router.backTo(Screen.Challenge.Single)
            }
            setCancelable(false)
        }.show()
    }

    private val callback = object : OnBackPressedCallback(false) {
        override fun handleOnBackPressed() {
            presenter.action()
            AlertDialog.Builder(requireContext()).apply {
                setTitle(R.string.label_exit_run_title)
                setMessage(R.string.label_exit_run_message)
                setNegativeButton(R.string.label_back) { d, _ ->
                    d.dismiss()
                    presenter.action()
                }
                setCancelable(false)
                setPositiveButton(R.string.label_lose) { d, _ ->
                    d.dismiss()
                    val activity = activity ?: return@setPositiveButton
                    val isBackStackEmpty = fragmentManager?.isBackStackEmpty ?: false
                    val skip = activity.isDestroyed || activity.isFinishing

                    if (isHidden || skip || (isBackStackEmpty && helper.invokedFromToolbar)) return@setPositiveButton

                    if (isBackStackEmpty) router.finishFlow() else router.backTo(Screen.Challenge.Single)
                }
            }.show()
        }
    }
}