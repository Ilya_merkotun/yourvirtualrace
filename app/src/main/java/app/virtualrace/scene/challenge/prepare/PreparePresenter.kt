package app.virtualrace.scene.challenge.prepare

import app.virtualrace.R
import app.virtualrace.core.common.*
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.ProgressManager
import app.virtualrace.core.presentation.presenter.ExceptionHandlingPresenter
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.data.Wrapper
import app.virtualrace.data.api.model.MyChallenge
import app.virtualrace.scene.Screen
import app.virtualrace.scene.main.provider.ChallengeData
import app.virtualrace.scene.main.provider.ChallengesStore
import app.virtualrace.scene.main.provider.RunningData
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PreparePresenter @Inject constructor(
    v: PrepareScene.View,
    job: Job,
    executor: Executor,
    humanizer: ExceptionHumanizer,
    resourceManager: ResourceManager,
    private val data: RunningData,
    private val flowRouter: FlowRouter,
    private val progressManager: ProgressManager,
    private val challengesStore: ChallengesStore,
    private val permissionManager: PermissionManager
) : ExceptionHandlingPresenter<PrepareScene.View>(
    v,
    job,
    executor,
    humanizer,
    resourceManager
), PrepareScene.Presenter,
    PermissionsCallback {

    private lateinit var challenge: MyChallenge

    companion object {
        const val TIME = 5
    }

    override fun start() {
        launch(executor.ui) {
            challenge = (challengesStore.challengeById(data.id) as ChallengeData.Joined).data
            view?.showTime(TIME, true, challenge.challenge.name)
            run()
        }
    }

    override fun run() {
        if (permissionManager.granted) action()
        else permissionManager.request(this, *PrepareScene.PERMISSIONS)
    }

    private fun action() {
        launch(executor.io) {
            for (i in TIME downTo 0) {
                withContext(executor.ui) {
                    view?.showTime(i, false, challenge.challenge.name)
                }
                delay(1000L)
            }
            withContext(executor.ui) {
                flowRouter.navigateTo(Screen.Challenge.Run)
            }
        }
    }

    override fun onPermissionsGranted(granted: Boolean) {
        if (granted) {
            action()
        } else {
            view?.message(
                resourceManager.getString(R.string.error_no_permission),
                Messageable.LENGTH_SHORT,
                Messageable.TOAST
            )
        }
    }

    override val handler = ExceptionHandler { _, exception ->
        progressManager.hideProgress()
        view?.message(humanizer.humanize(exception), Messageable.LENGTH_SHORT, Messageable.TOAST)
    }
}