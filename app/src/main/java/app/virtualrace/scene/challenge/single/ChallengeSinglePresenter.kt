package app.virtualrace.scene.challenge.single

import android.util.Log
import app.virtualrace.BuildConfig
import app.virtualrace.R
import app.virtualrace.common.Constant
import app.virtualrace.core.common.ExceptionHandler
import app.virtualrace.core.common.ExceptionHumanizer
import app.virtualrace.core.common.Executor
import app.virtualrace.core.common.ResourceManager
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.ProgressManager
import app.virtualrace.core.presentation.presenter.ExceptionHandlingPresenter
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.data.api.Api
import app.virtualrace.scene.Screen
import app.virtualrace.scene.main.provider.ChallengeData
import app.virtualrace.scene.main.provider.ChallengeType
import app.virtualrace.scene.main.provider.ChallengesStore
import app.virtualrace.scene.main.provider.RunningData
import app.virtualrace.util.TimeFormat
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class ChallengeSinglePresenter @Inject constructor(
    v: ChallengeSingleScene.View,
    job: Job,
    executor: Executor,
    humanizer: ExceptionHumanizer,
    resourceManager: ResourceManager,
    private val api: Api,
    private val flowRouter: FlowRouter,
    private val progressManager: ProgressManager,
    private val challengesStore: ChallengesStore,
    private val runningData: RunningData
) : ExceptionHandlingPresenter<ChallengeSingleScene.View>(
    v,
    job,
    executor,
    humanizer,
    resourceManager
), ChallengeSingleScene.Presenter {

    private var type: ChallengeType = ChallengeType.ALL
    private var data: ChallengeData? = null
    private var url: String? = null

    override fun start() {
        launch(executor.ui) {
            progressManager.showProgress()
            load()
            progressManager.hideProgress()
        }
    }

    private suspend fun load() {
        val challengeData = challengesStore.challengeById(runningData.id)
        data = challengeData
        val item = when (challengeData) {
            is ChallengeData.Joined -> {
                type = ChallengeType.JOINED
                challengeData.data.challenge
            }
            is ChallengeData.Completed -> {
                type = ChallengeType.COMPLETED
                challengeData.data.challenge
            }
            else -> {
                type = ChallengeType.ALL
                challengeData as ChallengeData.All
                challengeData.data
            }
        }

        val imageUlr =
            item.image?.let { BuildConfig.API_BASE_URL + "challenges/" + item.id + "/image" }
                ?: item.image
        view?.showInfo(
            item.name,
            item.distance,
            item.distanceType,
            item.countCompleted,
            item.countJoined,
            item.description,
            imageUlr,
            item.startDate,
            item.endDate,
            type
        )
        url = item.websiteUrl
        Log.i("Tag ", "$url")
        view?.showMoreInfo(url != null)
    }

    override fun moreInfo() {
        url?.let { view?.openUrl(it) }
    }

    override fun action() {
        launch(executor.ui) {
            data?.let {
                when (it) {
                    is ChallengeData.All -> {
                        if (TimeFormat.formatFromServerShort(it.data.endDate) > TimeFormat.calendar.time) {
                            val status = api.joinAsync(runningData.id).await()
                            if (status.status == Constant.STATUS_SUCCESS) {
                                challengesStore.refresh()
                                load()
                            } else {
                                //error
                            }
                        } else {
                            view?.message(
                                resourceManager.getString(R.string.error_challenge_over),
                                Messageable.LENGTH_SHORT,
                                Messageable.TOAST
                            )
                        }
                    }
                    is ChallengeData.Joined -> {
                        if (it.data.challenge.startDate != null
                            && (TimeFormat.formatFromServerShort(it.data.challenge.startDate) > TimeFormat.calendar.time)
                        ) {
                            view?.message(
                                resourceManager.getString(R.string.error_challenge_you_need_wait),
                                Messageable.LENGTH_SHORT,
                                Messageable.TOAST
                            )
                        } else
                            if (it.data.challenge.teams.isEmpty()) {
                                runningData.team = null
                                flowRouter.navigateTo(Screen.Challenge.Prepare)
                            } else flowRouter.navigateTo(Screen.Challenge.Team)
                    }
                    is ChallengeData.Completed -> {
                        //TODO: navigate to show all result
                        flowRouter.navigateTo(Screen.Challenge.Result(it.data.challengeId))
                    }
                }
            }
        }
    }

    override val handler = ExceptionHandler { _, exception ->
        progressManager.hideProgress()
        view?.message(humanizer.humanize(exception), Messageable.LENGTH_SHORT, Messageable.TOAST)
    }
}