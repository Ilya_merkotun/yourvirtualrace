package app.virtualrace.scene.challenge.prepare

import app.virtualrace.core.common.PermissionManager
import app.virtualrace.core.common.internal.AndroidPermissionManager
import app.virtualrace.core.di.module.CoroutineModule
import app.virtualrace.data.Wrapper
import app.virtualrace.data.wrap

class PrepareModule(f: PrepareFragment) : CoroutineModule(f) {
    init {
        bind(PrepareScene.View::class.java).toInstance(f)
        bind(PrepareScene.Presenter::class.java).to(PreparePresenter::class.java).singletonInScope()
        bind(PermissionManager::class.java)
            .toInstance(AndroidPermissionManager(f, *PrepareScene.PERMISSIONS))

//        bind(Wrapper.Int::class.java).toInstance(id.wrap())
    }
}