package app.virtualrace.scene.challenge.team

import android.view.ViewGroup
import app.virtualrace.R
import app.virtualrace.core.presentation.provider.Provider
import app.virtualrace.core.ui.adapter.delegate.BindingDelegate
import app.virtualrace.core.ui.adapter.viewholder.ViewHolder
import app.virtualrace.data.api.model.Team
import kotlinx.android.synthetic.main.item_team.view.*
import java.lang.ref.WeakReference

class TeamViewHolder(
    parent: ViewGroup,
    weakListener: WeakReference<out OnViewHolderClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(
    parent, weakListener, R.layout.item_team, true
) {
    class Delegate(
        provider: Provider<*>,
        listener: OnViewHolderClickListener?
    ) : BindingDelegate<Team, TeamViewHolder, OnViewHolderClickListener>(
        Team::class.java, provider, listener
    ) {
        override fun createViewHolder(parent: ViewGroup) = TeamViewHolder(parent, weakListener)

        override fun bindViewHolder(holder: TeamViewHolder, item: Team, position: Int) {
            with(holder.itemView) {
                buttonView.text = item.name
            }
        }
    }
}