package app.virtualrace.scene.challenge.single

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import app.virtualrace.R
import app.virtualrace.common.Constant
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.ui.HoldingToolbarTitle
import app.virtualrace.core.ui.StatusBarStyleable
import app.virtualrace.core.ui.VisibleBottomBar
import app.virtualrace.core.ui.VisibleToolbar
import app.virtualrace.core.ui.fragment.BaseFragment
import app.virtualrace.extension.load
import app.virtualrace.scene.main.provider.ChallengeType
import app.virtualrace.util.TimeFormat
import kotlinx.android.synthetic.main.fragment_challenge.*
import toothpick.Scope
import javax.inject.Inject

class ChallengeSingleFragment : BaseFragment(),
    VisibleToolbar,
    VisibleBottomBar,
    ScopeAware,
    ChallengeSingleScene.View,
    View.OnClickListener,
    StatusBarStyleable {

    companion object {
        fun newInstance() = ChallengeSingleFragment()
    }

    override var toolbarTitle: CharSequence?
        get() =
            (parentFragment as? HoldingToolbarTitle)?.titleTextView?.text
        set(value) {
            (parentFragment as? HoldingToolbarTitle)?.titleTextView?.text = value
        }
    override val isVisibleToolbar = true
    override val isVisibleBottomBar = false
    override val isLightStatusBar = true
    override val layoutResId = R.layout.fragment_challenge

    @Inject lateinit var presenter: ChallengeSingleScene.Presenter

    override fun onScopeCreated(scope: Scope) =
        scope.installModules(
            ChallengeSingleModule(this)
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        moreInfoButton.setOnClickListener(this)
        actionButton.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()

        presenter.start()
    }

    override fun showInfo(
        name: String,
        distance: Double,
        typeDistance: String?,
        currentParti: Int,
        alreadyRun: Int,
        description: String?,
        image: String?,
        startDate: String?,
        endDate: String,
        type: ChallengeType
    ) {
        distanceText.text = distance.toString() + typeDistance
        currentText.text = getString(R.string.format_people, currentParti)
        alreadyRunText.text = getString(R.string.format_people, alreadyRun)
        descriptionText.text = description
        imageView.load(image, R.drawable.vec_white_man, R.dimen.spacing)

        startDateText.text = startDate
            ?.let { TimeFormat.dd_MMMM_yyyy.format(TimeFormat.formatFromServerShort(it)) }
            ?: Constant.EMPTY

        endDateText.text = TimeFormat.dd_MMMM_yyyy.format(TimeFormat.formatFromServerShort(endDate))
        toolbarTitle = name

        when (type) {
            ChallengeType.ALL -> {
                actionButton.text = getString(R.string.label_join_challenge)
                actionButton.setBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.red
                    )
                )
            }
            ChallengeType.JOINED -> {
                actionButton.text = getString(R.string.label_run_challenge)
                actionButton.setBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.green
                    )
                )
            }
            ChallengeType.COMPLETED -> {
                actionButton.text = getString(R.string.label_view_results)
                actionButton.setBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.colorPrimary
                    )
                )
            }
        }
    }

    override fun showMoreInfo(isShow: Boolean) {
        if (isShow) {
            moreInfoButton.visibility = View.VISIBLE
        } else {
            moreInfoButton.visibility = View.GONE
        }
    }

    override fun openUrl(url: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(Intent.createChooser(intent, "Choose browser"))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.moreInfoButton -> presenter.moreInfo()
            R.id.actionButton -> presenter.action()
        }
    }
}