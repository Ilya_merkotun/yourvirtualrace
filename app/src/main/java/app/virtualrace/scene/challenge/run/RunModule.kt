package app.virtualrace.scene.challenge.run

import app.virtualrace.core.common.PermissionManager
import app.virtualrace.core.common.internal.AndroidPermissionManager
import app.virtualrace.core.di.module.CoroutineModule
import app.virtualrace.util.LocationTrackingService

class RunModule(f: RunFragment) : CoroutineModule(f) {
    init {
        LocationTrackingService.start(f.requireActivity(), 12345)

        bind(RunScene.View::class.java).toInstance(f)
        bind(RunScene.Presenter::class.java).to(RunPresenter::class.java).singletonInScope()
        bind(PermissionManager::class.java)
            .toInstance(AndroidPermissionManager(f, *RunScene.PERMISSIONS))
    }
}