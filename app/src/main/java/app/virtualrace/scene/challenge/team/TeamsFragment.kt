package app.virtualrace.scene.challenge.team

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import androidx.recyclerview.widget.RecyclerView
import app.virtualrace.R
import app.virtualrace.common.Constant
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.ui.*
import app.virtualrace.core.ui.adapter.DelegatedRecyclerAdapter
import app.virtualrace.core.ui.adapter.viewholder.ViewHolder
import app.virtualrace.core.ui.adapter.viewholder.viewTypeManagerOf
import app.virtualrace.core.ui.fragment.BaseListFragment
import app.virtualrace.core.widget.StateLayout
import app.virtualrace.data.api.model.Team
import kotlinx.android.synthetic.main.fragment_list_challenges.*
import toothpick.Scope
import javax.inject.Inject

class TeamsFragment : BaseListFragment<Team>(),
    ScopeAware,
    VisibleToolbar,
    VisibleBottomBar,
    TeamsScene.View,
    HoldingStateLayout,
    ViewHolder.OnViewHolderClickListener,
    StatusBarStyleable {

    companion object {
        fun newInstance() = TeamsFragment()
    }

    override val layoutResId = R.layout.fragment_list_teams
    override var toolbarTitle: CharSequence?
        get() =
            (parentFragment as? HoldingToolbarTitle)?.titleTextView?.text
        set(value) {
            (parentFragment as? HoldingToolbarTitle)?.titleTextView?.text = value
        }
    override val isVisibleToolbar = true
    override val isVisibleBottomBar = false
    override val isLightStatusBar = true
    override val stateLayout: StateLayout get() = syntheticStateLayout

    @Inject lateinit var presenter: TeamsScene.Presenter

    override fun onScopeCreated(scope: Scope) = scope.installModules(TeamsModule(this))

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.addItemDecoration(
            DividerItemDecoration(requireContext(), VERTICAL).apply {
                ContextCompat.getDrawable(requireContext(), R.drawable.divider)
                    ?.let { setDrawable(it) }
            })

        presenter.start()
    }

    override fun onAdapterCreated(adapter: DelegatedRecyclerAdapter<Team>) {
        super.onAdapterCreated(adapter)
        adapter += viewTypeManagerOf(
            adapter.provider,
            Team::class.java to adapter + TeamViewHolder.Delegate(adapter.provider, this)
        )
    }

    override fun showTeams(list: List<Team>) {
        adapter.provider.set(list)
        adapter.notifyDataSetChanged()
    }

    override fun setTitle(title: String?) {
        toolbarTitle = "Select ${title ?: Constant.EMPTY}"
    }

    override fun onViewHolderClick(holder: RecyclerView.ViewHolder, position: Int, id: Int) =
        presenter.navigateTo(position)
}