package app.virtualrace.scene.challenge.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import app.virtualrace.R
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.extension.isVisible
import app.virtualrace.core.location.Location
import app.virtualrace.core.ui.StatusBarStyleable
import app.virtualrace.core.ui.VisibleBottomBar
import app.virtualrace.core.ui.VisibleToolbar
import app.virtualrace.core.ui.fragment.BaseFragment
import app.virtualrace.util.TimeFormat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import kotlinx.android.synthetic.main.fragment_result.*
import toothpick.Scope
import java.math.BigDecimal
import java.math.RoundingMode
import javax.inject.Inject

class ResultFragment : BaseFragment(),
    ResultScene.View,
    VisibleToolbar,
    VisibleBottomBar,
    StatusBarStyleable,
    ScopeAware,
    OnMapReadyCallback {

    companion object {
        const val INITIAL_ZOOM = 17f
        private const val KEY_1 = ".KEY_1"
        fun newInstance(id: Int) = ResultFragment().apply {
            arguments = Bundle(1).also {
                it.putInt(KEY_1,id)
            }
        }
    }

    private lateinit var map: GoogleMap

    override val toolbarTitleRes = R.string.toolbar_congratulations
    override val layoutResId = R.layout.fragment_result
    override val isVisibleBottomBar = false
    override val isVisibleToolbar = true
    override val isLightStatusBar = true

    @Inject lateinit var presenter: ResultScene.Presenter

    override fun onScopeCreated(scope: Scope) =
        scope.installModules(ResultModule(this, requireArguments().getInt(KEY_1)))

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = super.onCreateView(inflater, container, savedInstanceState).also {
        (childFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment?)?.let {
            it.getMapAsync(this)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        completeButton.isVisible = false

        presenter.start()
    }

    override fun onMapReady(m: GoogleMap) {
        map = m
        map.uiSettings.setAllGesturesEnabled(true)
        presenter.updateMap()
    }

    override fun showInfo(distance: String?, pace: Double?, time: Int?) {
        distanceText.text = distance
        paceText.text = "${TimeFormat.formatFromSec( BigDecimal(pace!!).setScale(2, RoundingMode.HALF_EVEN).toInt())}/km "
        timeText.text = TimeFormat.formatFromSec(time!!)
    }

    override fun showPoints(list: List<Location>) {
        if(list.isEmpty()) return

        val bound = LatLngBounds.Builder()
        val polylineOptions = PolylineOptions()
            .color(ContextCompat.getColor(requireContext(), R.color.map))
            .width(4f)
        list.forEach {
            bound.include(LatLng(it.latitude, it.longitude))
            polylineOptions.add(LatLng(it.latitude, it.longitude))
        }

        map.animateCamera(CameraUpdateFactory.newLatLngBounds( bound.build(), 75))

        map.addMarker(MarkerOptions().position(LatLng(list.first().latitude, list.first().longitude)).title("Start"))
        map.addMarker(MarkerOptions().position(LatLng(list.last().latitude, list.last().longitude)).title("End"))

        map.addPolyline(polylineOptions)
    }
}