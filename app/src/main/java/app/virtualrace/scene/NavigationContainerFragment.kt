package app.virtualrace.scene

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.FragmentManager
import app.virtualrace.BuildConfig
import app.virtualrace.R
import app.virtualrace.core.common.OnBackPressedHelper
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.di.module.ContainerModule
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.core.ui.HoldingBottomBar
import app.virtualrace.core.ui.HoldingToolbarTitle
import app.virtualrace.core.ui.VisibleToolbar
import app.virtualrace.core.ui.fragment.container.BaseContainerFragment
import app.virtualrace.core.view.FittingSystemWindow
import ru.terrakok.cicerone.Router
import toothpick.Scope
import javax.inject.Inject

class NavigationContainerFragment : BaseContainerFragment(),
    ScopeAware,
    FragmentManager.OnBackStackChangedListener,
    FittingSystemWindow,
    HoldingBottomBar,
    HoldingToolbarTitle {

    companion object {
        private const val KEY_1 = BuildConfig.APPLICATION_ID + ".KEY_1"

        fun newInstance(screen: Screen.Navigation) = NavigationContainerFragment().apply {
            arguments = Bundle(1).also {
                it.putSerializable(KEY_1, screen)
            }
        }
    }

    override val layoutResId get() = R.layout.fragment_container
    override val containerId get() = R.id.container
    override val titleTextView get() = (parentFragment as HoldingToolbarTitle).titleTextView
    override val bottomView get() = (parentFragment as? HoldingBottomBar)?.bottomView

    @Inject
    lateinit var router: FlowRouter
    @Inject
    lateinit var helper: OnBackPressedHelper

    // region Lifecycle
    override fun onScopeCreated(scope: Scope) {
        scope.installModules(
            ContainerModule(
                FlowRouter(scope.parentScope.getInstance(Router::class.java)),
                FlowRouter::class.java
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (childFragmentManager.fragments.isEmpty()) {
            router.replaceScreen(arguments?.getSerializable(KEY_1) as Screen)
        }
    }
    // endregion

    // region FragmentManager.OnBackStackChangedListener
    override fun onBackStackChanged() {
        val bar = supportActivity?.supportActionBar ?: return
        val isBackAllowed =
            !childFragmentManager.isBackStackEmpty
                    && (childFragmentManager.findFragmentById(containerId) as?  VisibleToolbar)?.isBackAllowed
                    ?: true

        bar.setHomeButtonEnabled(isBackAllowed)
        bar.setDisplayHomeAsUpEnabled(isBackAllowed)

        callback.isEnabled = true
    }
    // endregion

    override fun didBecomeActive() {
        onBackStackChanged()

        supportActivity?.let {
            it.onBackPressedDispatcher.addCallback(this, callback)
            childFragmentManager.addOnBackStackChangedListener(this)
        }
    }

    override fun didResignActive() {
        childFragmentManager.removeOnBackStackChangedListener(this)

        callback.isEnabled = false
        callback.remove()
    }

    private val callback = object : OnBackPressedCallback(false) {
        override fun handleOnBackPressed() {
            val activity = activity ?: return
            val isBackStackEmpty = childFragmentManager.isBackStackEmpty
            val skip = activity.isDestroyed || activity.isFinishing

            if (isHidden || skip || (isBackStackEmpty && helper.invokedFromToolbar)) return

            if (isBackStackEmpty) router.finishFlow() else router.exit()
        }
    }
}