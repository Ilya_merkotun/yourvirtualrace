package app.virtualrace.scene.main.challenges.my

import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.Refreshable
import app.virtualrace.core.presentation.StateView
import app.virtualrace.core.presentation.common.Starting
import app.virtualrace.data.api.model.MyChallenge

interface ChallengesListScene {
    interface View : Attachable, Messageable, StateView , Refreshable {
        fun setChallenges(challenges: List<MyChallenge>)
    }

    interface Presenter : Starting {
        fun refresh()
        fun logout()
        fun navigateTo(position: Int)
    }
}