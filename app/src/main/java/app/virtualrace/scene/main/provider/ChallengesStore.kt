package app.virtualrace.scene.main.provider

import app.virtualrace.data.api.model.Challenge
import app.virtualrace.data.api.model.MyChallenge

interface ChallengesStore {
    suspend fun refresh()
    suspend fun allChallenges(): List<Challenge>
    suspend fun myChallenges(): List<MyChallenge>
    suspend fun challengeByPosition(position: Int, isMy: Boolean): Challenge?
    suspend fun challengeById(id: Int): ChallengeData?
}