package app.virtualrace.scene.main.challenges

import app.virtualrace.R
import app.virtualrace.account.AccountManager
import app.virtualrace.core.common.ExceptionHandler
import app.virtualrace.core.common.ExceptionHumanizer
import app.virtualrace.core.common.Executor
import app.virtualrace.core.common.ResourceManager
import app.virtualrace.core.extension.unit
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.ProgressManager
import app.virtualrace.core.presentation.StateView
import app.virtualrace.core.presentation.presenter.ExceptionHandlingPresenter
import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.scene.Screen
import app.virtualrace.scene.main.provider.ChallengesStore
import app.virtualrace.scene.main.provider.RunningData
import app.virtualrace.util.InternetConnection
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class ChallengesListPresenter @Inject constructor(
    v: ChallengesListScene.View,
    job: Job,
    executor: Executor,
    humanizer: ExceptionHumanizer,
    resourceManager: ResourceManager,
    private val router: FlowRouter,
    private val progressManager: ProgressManager,
    private val accountManager: AccountManager,
    private val challengesStore: ChallengesStore,
    private val internetConnection: InternetConnection,
    private val runningData: RunningData
) : ExceptionHandlingPresenter<ChallengesListScene.View>(
    v,
    job,
    executor,
    humanizer,
    resourceManager
), ChallengesListScene.Presenter {

    override fun start() = launch(executor.ui) {
        load()
    }.unit()

    private suspend fun load() {
        if (!internetConnection.isOnline) {
            view?.message(
                resourceManager.getString(R.string.error_no_inet),
                Messageable.LENGTH_SHORT,
                Messageable.SNACKBAR
            )
            return
        }
        view?.showState(StateView.State.Progress)

        view?.setChallenges(challengesStore.allChallenges())
        view?.showState(StateView.State.Content)
    }

    override fun navigateTo(position: Int) {
        launch(executor.ui) {
            val id = challengesStore.challengeByPosition(position, false)?.id ?: return@launch
            runningData.id = id
            router.navigateTo(Screen.Challenge.Single)
        }
    }

    override fun logout() {
        accountManager.remove()
        router.startFlow(Screen.Root.Authorization)
    }

    override fun refresh() {
        launch(executor.ui) {
            view?.isRefreshing = true
            load()
            view?.isRefreshing = false
        }.unit()
    }

    override val handler = ExceptionHandler { _, exception ->
        progressManager.hideProgress()
        view?.message(humanizer.humanize(exception), Messageable.LENGTH_SHORT, Messageable.TOAST)
    }
}