package app.virtualrace.scene.main

import app.virtualrace.core.di.module.ContainerModule
import app.virtualrace.scene.main.provider.ChallengesMemoryStore
import app.virtualrace.scene.main.provider.ChallengesStore
import app.virtualrace.scene.main.provider.RunningData
import app.virtualrace.scene.main.provider.RunningDataStore
import app.virtualrace.scene.main.roter.MainRouter
import ru.terrakok.cicerone.Router

class MainModule(f: MainFragment, router: Router) : ContainerModule<MainRouter>(
    MainRouter(router), MainRouter::class.java
) {
    init {
        bind(MainScene.View::class.java).toInstance(f)
        bind(MainScene.Presenter::class.java).to(MainPresenter::class.java).singletonInScope()

        bind(ChallengesStore::class.java).to(ChallengesMemoryStore::class.java).singletonInScope()
        bind(RunningData::class.java).toInstance(RunningDataStore())
    }
}