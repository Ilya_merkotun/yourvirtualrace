package app.virtualrace.scene.main.provider

import app.virtualrace.data.api.model.Challenge
import app.virtualrace.data.api.model.MyChallenge

sealed class ChallengeData {
    class All(val data: Challenge) : ChallengeData()
    class Joined(val data: MyChallenge) : ChallengeData()
    class Completed(val data: MyChallenge) : ChallengeData()
}

enum class ChallengeType {
    ALL, JOINED, COMPLETED;
}