package app.virtualrace.scene.main.roter


import app.virtualrace.core.presentation.router.FlowRouter
import app.virtualrace.scene.Screen
import ru.terrakok.cicerone.Router

class MainRouter(router: Router) : FlowRouter(router) {
    fun clearCurrentStack(screen: Screen) = executeCommands(ClearStackCommand(screen))
}