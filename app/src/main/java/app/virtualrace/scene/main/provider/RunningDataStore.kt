package app.virtualrace.scene.main.provider

import app.virtualrace.core.location.Location
import app.virtualrace.data.api.model.Team

class RunningDataStore : RunningData {
    override var id: Int = 0
        set(value) {
            clearRun()
            field = value
        }
    override var currentDistance: Int = 0
    override val averagePace: Double
        get() = currentTime.toDouble() / (currentDistance.toDouble() / 1000)
    override var currentTime: Int = 0
    override var points: MutableList<Location> = mutableListOf()
    override var type: String = ""
    override var team: Team? = null

    override fun clearRun() {
        currentDistance = 0
        currentTime = 0
        points.clear()
        type = ""
        team = null
    }
}