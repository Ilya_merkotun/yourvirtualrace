package app.virtualrace.scene.main

import app.virtualrace.R
import app.virtualrace.core.presentation.common.Starting

interface MainScene {
    interface View
    interface Presenter : Starting {
        fun select(@Navigation item: Int)
        fun reSelect(@Navigation item: Int)
    }

    @Retention(AnnotationRetention.SOURCE)
    annotation class Navigation {
        companion object {
            const val ALL = R.id.all
            const val MY = R.id.my
//            const val CURRENT = R.id.current
        }
    }
}