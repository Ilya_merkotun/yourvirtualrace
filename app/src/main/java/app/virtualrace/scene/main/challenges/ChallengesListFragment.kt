package app.virtualrace.scene.main.challenges

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.virtualrace.R
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.ui.HoldingStateLayout
import app.virtualrace.core.ui.StatusBarStyleable
import app.virtualrace.core.ui.VisibleBottomBar
import app.virtualrace.core.ui.VisibleToolbar
import app.virtualrace.core.ui.adapter.DelegatedRecyclerAdapter
import app.virtualrace.core.ui.adapter.viewholder.ViewHolder
import app.virtualrace.core.ui.adapter.viewholder.viewTypeManagerOf
import app.virtualrace.core.ui.fragment.BaseListFragment
import app.virtualrace.core.widget.StateLayout
import app.virtualrace.core.widget.recycler.DividerItemDecoration
import app.virtualrace.data.api.model.Challenge
import kotlinx.android.synthetic.main.fragment_list_challenges.*
import toothpick.Scope
import javax.inject.Inject

class ChallengesListFragment : BaseListFragment<Challenge>(),
    ScopeAware,
    VisibleToolbar,
    VisibleBottomBar,
    ChallengesListScene.View,
    HoldingStateLayout,
    ViewHolder.OnViewHolderClickListener,
    StatusBarStyleable,
    SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun newInstance() = ChallengesListFragment()
    }

    override val layoutResId = R.layout.fragment_list_challenges
    override val isVisibleToolbar = true
    override val isVisibleBottomBar = true
    override val isLightStatusBar = true
    override val toolbarTitleRes = R.string.toolbar_all_challenges

    override val optionsMenuRes = R.menu.logout

    override val stateLayout: StateLayout get() = syntheticStateLayout

    override var isRefreshing: Boolean
        get() = refreshLayout?.isRefreshing == true
        set(value) {
            refreshLayout?.isRefreshing = value
        }

    @Inject lateinit var presenter: ChallengesListScene.Presenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refreshLayout.setOnRefreshListener(this)
        recyclerView.addItemDecoration(DividerItemDecoration(requireContext()))
        presenter.start()
    }

    override fun onScopeCreated(scope: Scope) =
        scope.installModules(ChallengesListModule(this))

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.logout -> {
                presenter.logout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onAdapterCreated(adapter: DelegatedRecyclerAdapter<Challenge>) {
        super.onAdapterCreated(adapter)
        adapter += viewTypeManagerOf(
            adapter.provider,
            Challenge::class.java to adapter + ChallengeViewHolder.Delegate(adapter.provider, this)
        )
    }

    override fun onRefresh() = presenter.refresh()

    //region ChallengesListScene.View
    override fun setChallenges(challenges: List<Challenge>) {
        adapter.provider.set(challenges)
        adapter.notifyDataSetChanged()
    }

    override fun onViewHolderClick(holder: RecyclerView.ViewHolder, position: Int, id: Int) =
        presenter.navigateTo(position)
}
