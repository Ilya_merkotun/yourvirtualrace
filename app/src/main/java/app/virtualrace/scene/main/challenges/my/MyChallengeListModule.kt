package app.virtualrace.scene.main.challenges.my

import app.virtualrace.core.di.module.CoroutineModule

class MyChallengeListModule(f: MyChallengesListFragment) : CoroutineModule(f) {
    init {
        bind(ChallengesListScene.View::class.java).toInstance(f)
        bind(ChallengesListScene.Presenter::class.java).to(MyChallengeListPresenter::class.java)
            .singletonInScope()
    }
}