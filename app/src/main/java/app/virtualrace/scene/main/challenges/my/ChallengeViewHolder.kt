package app.virtualrace.scene.main.challenges.my

import android.view.ViewGroup
import app.virtualrace.BuildConfig
import app.virtualrace.R
import app.virtualrace.core.presentation.provider.Provider
import app.virtualrace.core.ui.adapter.delegate.BindingDelegate
import app.virtualrace.core.ui.adapter.viewholder.ViewHolder
import app.virtualrace.data.api.model.MyChallenge
import app.virtualrace.extension.load
import app.virtualrace.util.TimeFormat
import app.virtualrace.util.TimeFormat.dd_MMMM_yyyy
import kotlinx.android.synthetic.main.item_challenge.view.*
import java.lang.ref.WeakReference

class ChallengeViewHolder(
    parent: ViewGroup,
    weakListener: WeakReference<out OnViewHolderClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(
    parent, weakListener, R.layout.item_challenge, true
) {
    class Delegate(
        provider: Provider<*>,
        listener: OnViewHolderClickListener?
    ) : BindingDelegate<MyChallenge, ChallengeViewHolder, OnViewHolderClickListener>(
        MyChallenge::class.java, provider, listener
    ) {
        override fun createViewHolder(parent: ViewGroup) = ChallengeViewHolder(parent, weakListener)

        override fun bindViewHolder(holder: ChallengeViewHolder, item: MyChallenge, position: Int) {
            with(holder.itemView) {

                if(item.challenge == null) {
                    nameText.text = "error"
                    dateText.text =  "error"
                    descriptionText.text =  "error"
                    countPeopleText.text =  "error"
                    imageView.load(null, R.drawable.vec_white_man, R.dimen.spacing)
                }
                else
                with(item.challenge) {
                    nameText.text = name
                    dateText.text = dd_MMMM_yyyy.format(TimeFormat.formatFromServerShort(endDate.toString()))
                    descriptionText.text = description!!
                    countPeopleText.text = holder.context.getString(
                        R.string.format_count_people,
                        countCompleted,
                        countJoined
                    )
                    val imageUlr =
                        image?.let { BuildConfig.API_BASE_URL + "challenges/" + item.challengeId + "/image" }
                            ?: image

                    imageView.load(imageUlr, R.drawable.vec_white_man, R.dimen.spacing)
                }

            }
        }
    }
}