package app.virtualrace.scene.main.provider

import app.virtualrace.data.api.model.Team
import app.virtualrace.core.location.Location as CoreLocation

interface RunningData {
    var id: Int
    var currentDistance: Int//always meters
    val averagePace: Double  //always sec
    var currentTime: Int //always sec
    var points: MutableList<CoreLocation>
    var type: String
    var team: Team?
    fun clearRun()
}