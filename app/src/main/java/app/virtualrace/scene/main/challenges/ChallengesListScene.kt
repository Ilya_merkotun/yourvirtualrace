package app.virtualrace.scene.main.challenges

import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.Refreshable
import app.virtualrace.core.presentation.StateView
import app.virtualrace.core.presentation.common.Starting
import app.virtualrace.data.api.model.Challenge

interface ChallengesListScene  {
    interface View: Attachable, Messageable, StateView, Refreshable {
         fun setChallenges(challenges: List<Challenge>)
    }
    interface Presenter: Starting {
        fun refresh()
        fun logout()
        fun navigateTo(position: Int)
    }
}