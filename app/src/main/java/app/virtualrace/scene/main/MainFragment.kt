package app.virtualrace.scene.main

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import app.virtualrace.R
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.ui.HoldingBottomBar
import app.virtualrace.core.ui.HoldingToolbarTitle
import app.virtualrace.core.ui.fragment.container.BaseContainerFragment
import app.virtualrace.scene.Screen
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_main.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace
import toothpick.Scope
import javax.inject.Inject

class MainFragment : BaseContainerFragment(),
    ScopeAware,
    MainScene.View,
    HoldingToolbarTitle,
    HoldingBottomBar,
    BottomNavigationView.OnNavigationItemSelectedListener,
    BottomNavigationView.OnNavigationItemReselectedListener {
    companion object {
        fun newInstance() = MainFragment()
    }

    override val titleTextView: TextView get() = toolbarTextView
    override val bottomView: BottomNavigationView get() = bottomNavigationView
    override val layoutResId get() = R.layout.fragment_main
    override val containerId get() = R.id.container

    @Inject lateinit var presenter: MainScene.Presenter

    // region ScopeAware
    override fun onScopeCreated(scope: Scope) {
        scope.installModules(
            MainModule(this, scope.parentScope.getInstance(Router::class.java))
        )
    }
    // endregion

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        supportActivity?.setSupportActionBar(toolbar)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        bottomNavigationView.setOnNavigationItemReselectedListener(this)

        if (childFragmentManager.fragments.isEmpty()) {
            presenter.start()
        }
    }

    // region OnNavigationItemSelectedListener, OnNavigationItemReselectedListener
    override fun onNavigationItemSelected(item: MenuItem) =
        presenter.select(item.itemId).let { true }

    override fun onNavigationItemReselected(item: MenuItem) = presenter.reSelect(item.itemId)
    // endregion

    override val navigator: Navigator by lazy {
        object : SupportAppNavigator(activity, childFragmentManager, containerId) {
            override fun activityBack() = this@MainFragment.activityBack()
            override fun setupFragmentTransaction(
                command: Command,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction
            ) {
                fragmentTransaction.setReorderingAllowed(true)
                fragmentTransaction(command, currentFragment, nextFragment, fragmentTransaction)
            }

            override fun applyCommand(command: Command?) {
                val activity = activity
                if (activity == null || activity.isFinishing || activity.isDestroyed) return

                val manager = childFragmentManager

                when (command) {
                    is Replace -> {
                        val screen = command.screen as Screen
                        val target = manager.findFragmentByTag(screen.screenKey)
                            ?: createFragment(screen)

                        with(manager.beginTransaction()) {

                            val fragments = manager.fragments
                            for (fragment in fragments) {
                                if (fragment == target) continue

                                hide(fragment)
                            }

                            if (!target.isAdded) {
                                add(containerId, target, screen.screenKey)

                            } else {
                                show(target)
                            }

                            commit()
                        }
                    }
                    else -> super.applyCommand(command)
                }
            }
        }
    }
}