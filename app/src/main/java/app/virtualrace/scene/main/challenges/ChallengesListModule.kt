package app.virtualrace.scene.main.challenges

import app.virtualrace.core.di.module.CoroutineModule

class ChallengesListModule(f: ChallengesListFragment) : CoroutineModule(f) {
    init {
        bind(ChallengesListScene.View::class.java).toInstance(f)
        bind(ChallengesListScene.Presenter::class.java).to(ChallengesListPresenter::class.java)
            .singletonInScope()
    }
}