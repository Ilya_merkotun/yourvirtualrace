package app.virtualrace.scene.main.provider

import app.virtualrace.core.common.Executor
import app.virtualrace.data.ChallengeStatus
import app.virtualrace.data.api.Api
import app.virtualrace.data.api.model.Challenge
import app.virtualrace.data.api.model.MyChallenge
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ChallengesMemoryStore @Inject constructor(
    private val executor: Executor,
    private val api: Api
) : ChallengesStore {

    private val allChallengesList = mutableListOf<Challenge>()
    private val myChallengesList = mutableListOf<MyChallenge>()
    private val completeChallengesList = mutableListOf<MyChallenge>()

    override suspend fun refresh() {
        load()
    }

    override suspend fun allChallenges(): List<Challenge> {
        load()
        return allChallengesList
    }

    override suspend fun myChallenges(): List<MyChallenge> {
        load()
        return myChallengesList
    }

    private suspend fun load() =
        withContext(executor.network) {
            api.myChallengesAsync().await().results.let { let ->
                myChallengesList.clear()
                myChallengesList.addAll(let)

                completeChallengesList.clear()
                completeChallengesList.addAll(let.filter { it.status == ChallengeStatus.COMPLETE })
            }

            api.availableChallengesAsync().await().challenges.let {
                allChallengesList.clear()
                allChallengesList.addAll(it)
            }
        }

    override suspend fun challengeByPosition(position: Int, isMy: Boolean) =
        if (isMy) myChallengesList[position].challenge
        else allChallengesList[position]

    override suspend fun challengeById(id: Int): ChallengeData? {
        load()

        return completeChallengesList
            .find { it.challengeId == id }?.let { ChallengeData.Completed(it) }
            ?: myChallengesList.find { it.challengeId == id }?.let { ChallengeData.Joined(it) }
            ?: allChallengesList.find { it.id == id }?.let { ChallengeData.All(it) }
    }
}