package app.virtualrace.scene.main

import app.virtualrace.core.presentation.presenter.Presenter
import app.virtualrace.scene.Screen
import app.virtualrace.scene.Screen.Navigation
import app.virtualrace.scene.Screen.NavigationContainer
import app.virtualrace.scene.main.roter.MainRouter
import javax.inject.Inject

class MainPresenter @Inject constructor(
    view: MainScene.View,
    private val router: MainRouter
) : Presenter<MainScene.View>(view), MainScene.Presenter {

    override fun start() = select(MainScene.Navigation.ALL)
    override fun select(item: Int) = router.replaceScreen(screen(item))
    override fun reSelect(item: Int) = router.clearCurrentStack(screen(item))

    private fun screen(@MainScene.Navigation of: Int): Screen = when (of) {
        MainScene.Navigation.ALL -> NavigationContainer(Navigation.Main.AllChallenges)
        MainScene.Navigation.MY -> NavigationContainer(Navigation.Main.MyChallenges)
//        MainScene.Navigation.CURRENT -> NavigationContainer(Navigation.Main.MyRuns)
        else -> throw IllegalArgumentException()
    }
}