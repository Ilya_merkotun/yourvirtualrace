package app.virtualrace.util

import app.virtualrace.core.extension.weak
import app.virtualrace.core.location.Location
import java.lang.ref.WeakReference

class LocationServiceDataStorage {

    private var weakReference: WeakReference<LocationServiceListener?>? = null

    var location: Location? = null
        set(value) {
            field = value
            weakReference?.get()?.onLocationUpdate(value)
        }

    fun setListener(l: LocationServiceListener) {
        weakReference = l.weak()
    }

    fun removeListener() {
        weakReference = null
    }
}

interface LocationServiceListener {
    fun onLocationUpdate(location: Location?)
}