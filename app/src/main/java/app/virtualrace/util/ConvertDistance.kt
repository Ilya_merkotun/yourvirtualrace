package app.virtualrace.util

import java.math.BigDecimal
import java.math.RoundingMode

object ConvertDistance {
    const val MILES = "miles"
    const val KM = "km"
    const val M = "m"

    fun changeFormatDistance(distance: Double, isMilesOutPut: Boolean) =
        round(if (isMilesOutPut) distance / 1.60934 else distance * 1.60934)

    fun round(distance: Double) =
        BigDecimal(distance).setScale(3, RoundingMode.HALF_EVEN).toDouble()
}