package app.virtualrace.util

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object TimeFormat {
    val dd_MMMM_yyyy: DateFormat by lazy {
        SimpleDateFormat("dd MMMM yyyy", Locale.US)
    }
    private val yyyy_MM_dd_HH_mm_ss: DateFormat by lazy {
        //    2019-08-29 08:41:07
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
    }

    private val yyyy_MM_dd: DateFormat by lazy {
        //    2019-08-29
        SimpleDateFormat("yyyy-MM-dd", Locale.US)
    }

    private val HH_mm_ss: DateFormat by lazy {
        //  08:41:07
        SimpleDateFormat("HH:mm:ss", Locale.US)
    }

    val calendar = Calendar.getInstance()

    fun formatFromServer(date: String): Date = yyyy_MM_dd_HH_mm_ss.parse(date)
    fun formatFromServerShort(date: String): Date = yyyy_MM_dd.parse(date)

    fun formatFromSec(sec: Int) =
        "${twoDigitString(sec % 3600 / 60)}:${twoDigitString(sec % 60)}"

    private fun twoDigitString(number: Int) = when {
        number == 0 -> "00"
        number / 10 == 0 -> "0$number"
        else -> number.toString()
    }
}