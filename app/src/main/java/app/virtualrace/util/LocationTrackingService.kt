package app.virtualrace.util

import android.annotation.SuppressLint
import android.app.*
import android.app.NotificationManager.IMPORTANCE_HIGH
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import app.virtualrace.R
import app.virtualrace.core.common.ExceptionHandler
import app.virtualrace.core.common.Executor
import app.virtualrace.core.location.Location
import com.google.android.gms.location.*
import kotlinx.coroutines.*
import toothpick.Toothpick
import javax.inject.Inject

@SuppressLint("MissingPermission")
class LocationTrackingService : Service(), CoroutineScope {

    companion object {
        private const val TAG = "LocationTrackingService"
        private const val FOREGROUND_ID = 0x1404
        private const val ACCURACY = 15

        private const val KEY_0 = "LTS.KEY_0" // currentTrackingId
        private const val NO_ID = -1

        private const val CHANNEL_ID = "app.virtualrace.notification_tracking"
        private const val CHANNEL_NAME = "Location Tracking Channel"
        private const val CHANNEL_DESCRIPTION = "Notification channel for location tracking"

        var isRunning = false
            private set

        fun start(context: Context, trackingId: Int) {
            val intent = Intent(context, LocationTrackingService::class.java)
                .putExtra(KEY_0, trackingId)
            context.startService(intent)
        }

        fun stop(context: Context) = context.stopService(
            Intent(context, LocationTrackingService::class.java)
        )
    }

    private val job = SupervisorJob()

    override val coroutineContext by lazy { job + handler }

    @Inject lateinit var executor: Executor
    @Inject lateinit var client: FusedLocationProviderClient
    @Inject lateinit var manager: NotificationManagerCompat
    @Inject lateinit var serviceDateStorage: LocationServiceDataStorage

    private var currentTrackingId = NO_ID

    override fun onCreate() {
        isRunning = true
        val scope = Toothpick.openScopes(application, this)
        super.onCreate()

        Toothpick.inject(this, scope)
        createNotificationChannelIfNeeded()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if (intent != null) {
            currentTrackingId = intent.getIntExtra(KEY_0, NO_ID)

        } else runBlocking {
            updateCurrentTrackingId()
        }

        startForegroundNotification()
        listenLocationUpdates()

        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        isRunning = false
        client.removeLocationUpdates(locationCallback)
        job.cancel()
    }

    override fun onBind(intent: Intent?): IBinder? = null

    // region Internal

    private suspend fun updateCurrentTrackingId() {
        currentTrackingId = NO_ID
    }

    private fun createNotificationChannelIfNeeded() =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, IMPORTANCE_HIGH)
            channel.description = CHANNEL_DESCRIPTION
            channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

            manager.createNotificationChannel(channel)

        } else Unit

    private fun startForegroundNotification() {
        val intent = Intent(this, LauncherActivity::class.java)
        val contentIntent = PendingIntent.getActivity(
            this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT
        )

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.vec_logout)
            .setContentIntent(contentIntent)
            .setContentText("location tracking")
            .setOngoing(true)
            .build()

        manager.notify(FOREGROUND_ID, notification)
        startForeground(FOREGROUND_ID, notification)
    }

    private fun listenLocationUpdates() {
        val request = LocationRequest().apply {
            interval = 1001
            fastestInterval = 1002 / 2
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        client.requestLocationUpdates(request, locationCallback, null)

        launch(job) {
            while (true) {
                delay(1000L)
                client.lastLocation.addOnCompleteListener { task ->
                    task.result?.let {
                        if (it.accuracy > ACCURACY) return@let
                        serviceDateStorage.location =
                            Location(it.latitude, it.longitude, speed = it.speed)
                    }
                }
            }
        }
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult) {
            val location = result.lastLocation
            serviceDateStorage.location =
                Location(location.latitude, location.longitude, speed = location.speed)
        }

        override fun onLocationAvailability(availability: LocationAvailability) = Unit
    }

    private val handler = ExceptionHandler { _, e ->
        Log.d(TAG, "An error occurred: ${e.localizedMessage}")
        e.printStackTrace()
    }
    // endregion

    interface Listener {
        fun onLocationUpdate(location: Location)
    }
}