package app.virtualrace.util

import android.content.Context
import android.net.ConnectivityManager
import javax.inject.Inject

class InternetConnection @Inject constructor(private val context: Context) {
    val isOnline: Boolean
        get() {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?

            return (cm?.activeNetworkInfo != null
                    && cm.activeNetworkInfo.isAvailable
                    && cm.activeNetworkInfo.isConnected)
        }
}