package app.virtualrace.util

import android.content.SharedPreferences
import app.virtualrace.account.Account
import com.google.gson.Gson

class AppPreferences(
    private val preferences: SharedPreferences,
    private val gson: Gson
) : Preferences {
    private object Key {
        const val KEY_1 = "AC.KEY_1" // Account
    }

    override var account: Account
        get() = parse()
        set(value) {
            preferences.edit().apply {
                if (value is Account.Authorized) {
                    putString(Key.KEY_1, gson.toJson(value))
                } else {
                    remove(Key.KEY_1)
                }

            }.apply()
        }

    private fun parse(json: String? = preferences.getString(Key.KEY_1, null)): Account {
        return json?.let { gson.fromJson(it, Account.Authorized::class.java) }
            ?: Account.Unauthorized
    }
}