package app.virtualrace.util

import app.virtualrace.account.Account

interface Preferences {
    var account: Account
}