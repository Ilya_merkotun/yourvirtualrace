package app.virtualrace.app

import android.app.Application
import app.virtualrace.BuildConfig
import app.virtualrace.network.NetworkModule
import com.onesignal.OneSignal
import toothpick.Toothpick
import toothpick.configuration.Configuration

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        val configuration = if (!BuildConfig.DEBUG) {
            Configuration.forProduction()

        } else Configuration.forDevelopment().preventMultipleRootScopes()

        Toothpick.setConfiguration(configuration)

        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()

        Toothpick.openScope(this)
            .installModules(ApplicationModule(this), NetworkModule())

        registerActivityLifecycleCallbacks(ApplicationActivityLifecycleCallbacks())
    }
}