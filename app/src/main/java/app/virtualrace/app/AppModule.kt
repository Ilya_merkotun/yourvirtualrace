package app.virtualrace.app

import android.app.Application
import android.content.Context
import android.preference.PreferenceManager
import androidx.core.app.NotificationManagerCompat
import app.virtualrace.account.AccountManager
import app.virtualrace.account.AppAccountManager
import app.virtualrace.common.internal.AndroidExceptionHumanizer
import app.virtualrace.common.internal.activity.ActivityResultManager
import app.virtualrace.core.common.ExceptionHumanizer
import app.virtualrace.core.common.Executor
import app.virtualrace.core.common.ResourceManager
import app.virtualrace.core.common.internal.AndroidExecutor
import app.virtualrace.core.common.internal.AndroidResourceManager
import app.virtualrace.core.common.internal.LifecycleAwareNavigationHolder
import app.virtualrace.util.AppPreferences
import app.virtualrace.util.LocationServiceDataStorage
import app.virtualrace.util.Preferences
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.gson.Gson
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router
import toothpick.config.Module
import javax.inject.Inject
import javax.inject.Provider

class ApplicationModule(application: Application) : Module() {
    init {
        bind(Context::class.java).toInstance(application)
        bind(Application::class.java).toInstance(application)
        bind(Executor::class.java).toInstance(AndroidExecutor())
        bind(ResourceManager::class.java).toInstance(AndroidResourceManager(application.resources))

        bind(Preferences::class.java).toProvider(PreferencesProvider::class.java).singletonInScope()
        bind(AccountManager::class.java).to(AppAccountManager::class.java).singletonInScope()
        bind(ExceptionHumanizer::class.java)
            .to(AndroidExceptionHumanizer::class.java)
            .singletonInScope()

        bind(ActivityResultManager::class.java).singletonInScope()

        // Navigation
        Cicerone.create().let { cicerone ->
            bind(Router::class.java).toInstance(cicerone.router)
            bind(LifecycleAwareNavigationHolder::class.java)
                .toInstance(LifecycleAwareNavigationHolder(cicerone.navigatorHolder))
        }

        bind(FusedLocationProviderClient::class.java).toInstance(FusedLocationProviderClient(application))
        bind(NotificationManagerCompat::class.java).toInstance(NotificationManagerCompat.from(application))
        bind(LocationServiceDataStorage::class.java).toInstance(LocationServiceDataStorage())
    }
}

class PreferencesProvider @Inject constructor(
    private val context: Context,
    private val gson: Gson
) : Provider<Preferences> {
    override fun get() =
        AppPreferences(PreferenceManager.getDefaultSharedPreferences(context), gson)
}