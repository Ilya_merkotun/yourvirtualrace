package app.virtualrace.app

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import app.virtualrace.core.di.fragment.ScopeAwareLifecycleCallbacks
import app.virtualrace.core.view.FittingSystemWindowCallback

class ApplicationActivityLifecycleCallbacks : Application.ActivityLifecycleCallbacks {
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        if (activity is FragmentActivity) {
            activity.supportFragmentManager.apply {
                registerFragmentLifecycleCallbacks(FittingSystemWindowCallback, true)
                registerFragmentLifecycleCallbacks(ScopeAwareLifecycleCallbacks, true)
            }
        }
    }

    override fun onActivityStarted(activity: Activity) = Unit
    override fun onActivityResumed(activity: Activity) = Unit
    override fun onActivityPaused(activity: Activity) = Unit
    override fun onActivityStopped(activity: Activity) = Unit
    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) = Unit
    override fun onActivityDestroyed(activity: Activity) = Unit
}