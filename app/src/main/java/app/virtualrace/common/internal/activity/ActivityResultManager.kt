package app.virtualrace.common.internal.activity

import android.content.Intent
import app.virtualrace.core.data.weakCollectionOf
import javax.inject.Inject

class ActivityResultManager @Inject constructor() : ActivityResultCallback {

    private val delegates = weakCollectionOf<ActivityResultCallback>()

    fun add(callback: ActivityResultCallback) = delegates.add(callback)
    fun remove(callback: ActivityResultCallback) = delegates.remove(callback)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        for (delegate in delegates) {
            if (delegate.onActivityResult(requestCode, resultCode, data)) return true
        }
        return false
    }

}