package app.virtualrace.common.internal

import app.virtualrace.common.Constant
import app.virtualrace.core.common.ExceptionHumanizer
import app.virtualrace.data.api.model.ErrorResponse
import com.google.gson.Gson
import retrofit2.HttpException
import javax.inject.Inject

class AndroidExceptionHumanizer @Inject constructor(
    private val gson: Gson
) : ExceptionHumanizer {
    override fun humanize(throwable: Throwable): String =
        when (throwable) {
            is HttpException -> gson
                .fromJson(
                    throwable.response().errorBody()?.string(), ErrorResponse::class.java
                )?.error ?: Constant.EMPTY
            else -> throwable.localizedMessage
        }
}