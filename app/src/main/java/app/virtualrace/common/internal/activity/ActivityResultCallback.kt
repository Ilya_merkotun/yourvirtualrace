package app.virtualrace.common.internal.activity

import android.content.Intent

interface ActivityResultCallback {
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean
}