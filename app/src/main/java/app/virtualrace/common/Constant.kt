package app.virtualrace.common

object Constant {
    const val AUTHORIZATION = "Authorization"

    const val STATUS_OK = "ok"
    const val STATUS_ERROR = "error"
    const val STATUS_SUCCESS = "success"

    const val EMPTY = ""
    fun bearer(token: String?): String = "Bearer $token"
}