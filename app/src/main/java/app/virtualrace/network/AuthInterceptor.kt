package app.virtualrace.network

import app.virtualrace.account.Account
import app.virtualrace.account.AccountManager
import app.virtualrace.common.Constant
import app.virtualrace.core.common.Executor
import app.virtualrace.scene.Screen
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import ru.terrakok.cicerone.Router
import java.io.IOException
import javax.inject.Inject

class AuthInterceptor @Inject constructor(
    private val manager: AccountManager,
    private val router: Router,
    private val executor: Executor,
    private val accountManager: AccountManager
) : Interceptor,
    CoroutineScope {

    private fun authenticate(request: Request): Request = request.newBuilder().apply {
        val account = manager.account
        if (account is Account.Authorized) {
            header(HttpHeader.AUTHORIZATION, Constant.bearer(account.token))
        }
    }.build()

    override val coroutineContext by lazy { SupervisorJob() }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = authenticate(chain.request())
        val a = chain.proceed(request)
        val header = a.header("Content-Type")
        return if (header == "application/json")
             a
        else {
            launch(executor.ui) {
                accountManager.remove()
                router.replaceScreen(Screen.Root.Authorization)
            }
            throw IOException("You must re-login")
        }
    }
}