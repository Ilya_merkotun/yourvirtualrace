package app.virtualrace.network

import app.virtualrace.BuildConfig
import app.virtualrace.data.api.Api
import app.virtualrace.util.InternetConnection
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import toothpick.config.Module
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Provider

val gson: Gson by lazy {
    GsonBuilder()
        .setLenient()
        .serializeNulls()
        .create()
}

class NetworkModule : Module() {
    init {
        bind(Interceptor::class.java).to(AuthInterceptor::class.java).singletonInScope()
        bind(Gson::class.java).toInstance(gson)

        bind(OkHttpClient::class.java)
            .toProvider(OkHttpClientProvider::class.java)
            .providesSingletonInScope()
        bind(Retrofit::class.java)
            .toProvider(RetrofitProvider::class.java)
            .providesSingletonInScope()

        bind(Api::class.java)
            .toProvider(ApiProvider::class.java)
            .providesSingletonInScope()

        bind(InternetConnection::class.java).to(InternetConnection::class.java).singletonInScope()
    }
}

class RetrofitProvider @Inject constructor(
    private val client: OkHttpClient,
    private val gson: Gson
) : Provider<Retrofit> {
    override fun get(): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.API_BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
}

class OkHttpClientProvider @Inject constructor(
    private val authenticator: Interceptor
) : Provider<OkHttpClient> {
    override fun get(): OkHttpClient = OkHttpClient.Builder()
        .readTimeout(30, TimeUnit.SECONDS)
        .apply {
            addInterceptor(authenticator)
            if (BuildConfig.DEBUG) {
                addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            }
        }.build()
}

class ApiProvider @Inject constructor(
    private val retrofit: Retrofit
) : Provider<Api> {
    override fun get(): Api = retrofit.create(Api::class.java)
}