package app.virtualrace.extension

import android.location.Location
import app.virtualrace.core.location.Location as CoreLocation

/**
 * @return the approximate distance in meters
 */
fun CoreLocation.distanceTo(dest: CoreLocation) =
    this.toAndroidLocation("").distanceTo(dest.toAndroidLocation(""))

fun CoreLocation.toAndroidLocation(provider: String) = Location(provider).apply {
    longitude = this@toAndroidLocation.longitude
    latitude = this@toAndroidLocation.latitude
    altitude = this@toAndroidLocation.altitude
}