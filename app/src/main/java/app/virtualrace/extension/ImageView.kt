package app.virtualrace.extension

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.annotation.DimenRes
import androidx.core.view.setPadding
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory

private val ImageView.glide: RequestManager get() = Glide.with(this)

fun ImageView.load(
    what: String?,
    fallbackId: Int = 0,
    isCircleCrop: Boolean
) {
    glide.load(what)
        .apply {
            transition(
                DrawableTransitionOptions.withCrossFade(
                    DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build()
                )
            )
            if (fallbackId != 0) fallback(fallbackId)
            if (isCircleCrop) transform(CircleCrop())
        }
        .into(this)
}

fun ImageView.load(
    what: String?,
    fallbackId: Int = 0,
    @DimenRes padding: Int = 0
) {
    glide.load(what)
        .apply {
            transition(
                DrawableTransitionOptions.withCrossFade(
                    DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build()
                )
            )
            if (fallbackId != 0) fallback(fallbackId)
            if (padding != 0)
                listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        this@load.setPadding(
                            this@load.context.resources.getDimensionPixelSize(padding)
                        )
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        this@load.setPadding(0)
                        return false
                    }
                })
            centerCrop()
        }
        .into(this)
}