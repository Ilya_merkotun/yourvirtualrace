package app.virtualrace.data

object ChallengeStatus {
    const val COMPLETE = "complete"
    const val JOINED = "joined"
}