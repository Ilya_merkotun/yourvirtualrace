package app.virtualrace.data

sealed class Result<out T> {
    class Success<out T>(val data: T) : Result<T>()
    class Failure(val stringRes: Int) : Result<Nothing>()
}