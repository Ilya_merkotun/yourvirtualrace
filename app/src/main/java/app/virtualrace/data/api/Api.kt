package app.virtualrace.data.api

import app.virtualrace.data.api.model.*
import kotlinx.coroutines.Deferred
import retrofit2.http.*

interface Api {
    object Key {
        const val EMAIL = "email"
        const val PASSWORD = "password"
        const val FIRST_NAME = "first_name"
        const val LAST_NAME = "last_name"
        const val PIN = "pin"
        const val NAME = "name"
        const val DISTANCE = "distance"
        const val DISTANCE_TYPE = "distance_type"
        const val DESCRIPTION = "description"
        const val DESCRIPTION_SHORT = "description_short"
        const val WEB_SITE_URL = "website_url"
        const val START_DATE = "start_date"
        const val END_DATE = "end_date"
        const val CREATED_AT = "created_at"
        const val UPDATED_AT = "updated_at"
        const val JOINED_AT = "joined_at"
        const val COMPLETED_AT = "completed_at"
        const val IMAGE = "image"
        const val COUNT_COMPLETED = "count_completed"
        const val COUNT_JOINED = "count_joined"
        const val TEAMS = "teams"
        const val TEAM_TYPE = "team_type"
        const val ID = "id"
        const val USER_ID = "user_id"
        const val CHALLENGE_ID = "challenge_id"
        const val TEAM_ID = "team_id"
        const val AVERAGE_PACE = "average_pace"
        const val TIME = "time"
        const val RESULT_POINTS = "result_points"
        const val DEVICE_ID = "device_id"
    }

    object Path {
        object User {
            const val LOGIN = "user/login"
            const val REGISTER = "user/register"
            const val FORGOTTEN_PASSWORD = "user/forgotten"
            const val FORGOTTEN_PASSWORD_UPDATE = "user/forgotten/update"
        }

        object Challenge {
            const val MY = "challenges/my"
            const val AVAILABLE = "challenges/available"
            const val JOIN = "challenges/{${Key.ID}}/join"
            const val COMPLETE = "challenges/{${Key.ID}}/complete"
        }
    }

    // region Authorization
    @POST(Path.User.LOGIN)
    @FormUrlEncoded
    fun loginAsync(@Field(Key.EMAIL) email: String, @Field(Key.PASSWORD) password: String, @Field(Key.DEVICE_ID) deviceId:String): Deferred<SignInResponse>

    @POST(Path.User.REGISTER)
    @FormUrlEncoded
    fun registerAsync(
        @Field(Key.FIRST_NAME) firstName: String,
        @Field(Key.LAST_NAME) lastName: String,
        @Field(Key.EMAIL) email: String,
        @Field(Key.PASSWORD) password: String,
        @Field(Key.DEVICE_ID) deviceId:String
    ): Deferred<SignInResponse>

    @POST(Path.User.FORGOTTEN_PASSWORD)
    @FormUrlEncoded
    fun forgottenAsync(
        @Field(Key.EMAIL) email: String
    ): Deferred<Status>

    @POST(Path.User.FORGOTTEN_PASSWORD_UPDATE)
    @FormUrlEncoded
    fun forgottenUpdateAsync(
        @Field(Key.EMAIL) email: String,
        @Field(Key.PASSWORD) password: String,
        @Field(Key.PIN) pin: String
    ): Deferred<Status>
    //endregion

    // region Challenge list
    @GET(Path.Challenge.MY)
    fun myChallengesAsync(): Deferred<MyChallengesResponse>

    @GET(Path.Challenge.AVAILABLE)
    fun availableChallengesAsync(): Deferred<ChallengesResponse>
    //endregion

    //region Challenge
    @POST(Path.Challenge.JOIN)
    fun joinAsync(@retrofit2.http.Path(Key.ID) id: Int): Deferred<Status>

    @POST(Path.Challenge.COMPLETE)
    fun completeAsync(
        @retrofit2.http.Path(Key.ID) id: Int,
        @Body completeBody: CompleteBody
    ): Deferred<Status>
    //endregion
}