package app.virtualrace.data.api.model

class SignInResponse(
    val status: String,
    val token: String,
    val message: String
)