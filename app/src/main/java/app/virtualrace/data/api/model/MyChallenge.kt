package app.virtualrace.data.api.model

import app.virtualrace.data.api.Api
import com.google.gson.annotations.SerializedName

class MyChallenge(
    val id: Int,
    @SerializedName(Api.Key.USER_ID)
    val userId: Int,
    @SerializedName(Api.Key.CHALLENGE_ID)
    val challengeId: Int,
    @SerializedName(Api.Key.TEAM_ID)
    val teamID: Int?,
    val distance: Double?,
    val time: Int?,
    val status: String,
    @SerializedName(Api.Key.CREATED_AT)
    val createdAtDate: String?,
    @SerializedName(Api.Key.UPDATED_AT)
    val updatedAtDate: String?,
    @SerializedName(Api.Key.JOINED_AT)
    val joinedAtDate: String?,
    @SerializedName(Api.Key.COMPLETED_AT)
    val completedAtDate: String?,
    @SerializedName(Api.Key.AVERAGE_PACE)
    val averagePace: Double? = null,
    val challenge: Challenge,
    val points: List<Point>
)