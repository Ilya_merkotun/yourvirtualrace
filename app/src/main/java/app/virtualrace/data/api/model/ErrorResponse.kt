package app.virtualrace.data.api.model

class ErrorResponse(
    val error: String
)