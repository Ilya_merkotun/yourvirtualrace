package app.virtualrace.data.api.model

import app.virtualrace.data.api.Api
import com.google.gson.annotations.SerializedName

class UserRegister(
    @SerializedName(Api.Key.EMAIL)
    val email: String,
    @SerializedName(Api.Key.PASSWORD)
    val password: String,
    @SerializedName(Api.Key.FIRST_NAME)
    val firstName: String,
    @SerializedName(Api.Key.LAST_NAME)
    val lastName: String
)