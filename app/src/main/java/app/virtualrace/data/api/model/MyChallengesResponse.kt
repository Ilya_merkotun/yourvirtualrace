package app.virtualrace.data.api.model

class MyChallengesResponse(
    val status: String,
    val results: List<MyChallenge>
)