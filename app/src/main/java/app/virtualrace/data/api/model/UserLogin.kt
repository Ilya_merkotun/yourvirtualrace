package app.virtualrace.data.api.model

import app.virtualrace.data.api.Api
import com.google.gson.annotations.SerializedName

class UserLogin(
    @SerializedName(Api.Key.EMAIL)
    val email: String,
    @SerializedName(Api.Key.PASSWORD)
    val password: String
)