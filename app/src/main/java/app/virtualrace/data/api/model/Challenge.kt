package app.virtualrace.data.api.model

import app.virtualrace.data.api.Api
import com.google.gson.annotations.SerializedName

class Challenge(
    val id: Int,
    val name: String,
    val distance: Double,
    @SerializedName(Api.Key.DISTANCE_TYPE)
    val distanceType: String,
    val description: String?,
    @SerializedName(Api.Key.DESCRIPTION_SHORT)
    val descriptionShort: String?,
    @SerializedName(Api.Key.WEB_SITE_URL)
    val websiteUrl: String?,
    @SerializedName(Api.Key.START_DATE)
    val startDate: String?,
    @SerializedName(Api.Key.END_DATE)
    val endDate: String,
    @SerializedName(Api.Key.CREATED_AT)
    val createdAtDate: String,
    @SerializedName(Api.Key.UPDATED_AT)
    val updatedAtDate: String,
    val image: String?,
    @SerializedName(Api.Key.COUNT_COMPLETED)
    val countCompleted: Int,
    @SerializedName(Api.Key.COUNT_JOINED)
    val countJoined: Int,
    val teams: List<Team>,
    @SerializedName(Api.Key.TEAM_TYPE)
    val teamType: String?
)