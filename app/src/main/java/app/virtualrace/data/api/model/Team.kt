package app.virtualrace.data.api.model

import app.virtualrace.data.api.Api
import com.google.gson.annotations.SerializedName

class Team (
    val id : Int,
    @SerializedName(Api.Key.CHALLENGE_ID)
    val challengeId: Int,
    val name: String
)