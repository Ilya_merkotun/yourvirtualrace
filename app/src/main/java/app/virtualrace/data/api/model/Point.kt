package app.virtualrace.data.api.model

class Point(
    val id: Int,
    val lat: Double,
    val lon: Double
)

//"id": 610,
//"result_id": 40,
//"lat": "47.835064",
//"lon": "35.144546",
//"created_at": "2019-09-06 11:53:20",
//"updated_at": "2019-09-06 11:53:20"