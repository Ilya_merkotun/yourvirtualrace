package app.virtualrace.data.api.model

open class ChallengesResponse (
    open val status: String,
    open val challenges: List<Challenge>
)