package app.virtualrace.data.api.model

import app.virtualrace.data.api.Api
import com.google.gson.annotations.SerializedName

class CompleteBody(
    @SerializedName(Api.Key.DISTANCE) val distance: Float,
    @SerializedName(Api.Key.TEAM_ID) val teamId: Int?,
    @SerializedName(Api.Key.TIME) val time: Int,
    @SerializedName(Api.Key.AVERAGE_PACE) val averagePace: Float,
    @SerializedName(Api.Key.RESULT_POINTS) val resultsPoints: List<String>
)