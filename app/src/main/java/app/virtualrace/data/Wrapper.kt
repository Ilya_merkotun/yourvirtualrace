package app.virtualrace.data

sealed class Wrapper<T>(val value: T) {
    class Int(value: kotlin.Int) : Wrapper<kotlin.Int>(value)
    class String(value: kotlin.String) : Wrapper<kotlin.String>(value)
    class Boolean(value: kotlin.Boolean) : Wrapper<kotlin.Boolean>(value)
}

fun Int.wrap() = Wrapper.Int(this)
fun String.wrap() = Wrapper.String(this)
fun Boolean.wrap() = Wrapper.Boolean(this)

fun <T> Wrapper<T>.isNull() = value == null
fun <T> Wrapper<T>.isNotNull() = !isNull()