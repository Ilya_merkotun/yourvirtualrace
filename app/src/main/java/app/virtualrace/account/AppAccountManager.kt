package app.virtualrace.account

import app.virtualrace.util.Preferences
import javax.inject.Inject

class AppAccountManager @Inject constructor(
    private val preferences: Preferences
) : AccountManager {
    override var tempEmail: String? = null

    override val account: Account get() = preferences.account

    override fun save(account: Account) {
        preferences.account = account
    }

    override fun remove() {
        save(Account.Unauthorized)
    }
}