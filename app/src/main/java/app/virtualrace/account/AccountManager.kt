package app.virtualrace.account

interface AccountManager {
    val account: Account
    var tempEmail: String?

    fun save(account: Account)
    fun remove()
}