package app.virtualrace.account

sealed class Account {
    open class Authorized(
        val token: String
    ) : Account()

    object Unauthorized : Account()
}