package app.virtualrace.core.coroutine

import app.virtualrace.core.common.Executor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job

interface CoroutineAware : CoroutineScope {
    val job: Job
    val executor: Executor

    override val coroutineContext get() = executor.ui + job
}