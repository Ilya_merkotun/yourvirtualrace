package app.virtualrace.core.ui.adapter.viewholder

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

interface ViewHolderCreator {
    fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
}