package app.virtualrace.core.ui.adapter.viewholder

import app.virtualrace.core.presentation.provider.Provider
import app.virtualrace.core.ui.adapter.DelegatedAdapter
import app.virtualrace.core.ui.adapter.delegate.BindingDelegate

internal class ViewTypeManagerImpl(val provider: Provider<*>, viewTypes: Map<Class<*>, Int>) : ViewTypeManager {
    private val viewTypes: MutableMap<Class<*>, Int> = viewTypes.toMutableMap()

    override fun getItemViewType(position: Int, count: Int): Int {
        val item = provider[position] ?: throw IllegalArgumentException()
        val itemClass = item::class.java
        var viewType = viewTypes[itemClass]
        if (viewType == null) {
            val itemSuperType = viewTypes.keys.firstOrNull { it.isAssignableFrom(itemClass) }
            if (itemSuperType != null) {
                viewType = viewTypes[itemSuperType]
            }
        }
        return viewType ?: throw IllegalArgumentException()
    }

    operator fun plusAssign(viewTypes: Map<Class<*>, Int>) {
        this.viewTypes += viewTypes
    }
}

fun viewTypeManagerOf(provider: Provider<*>, viewTypes: Map<Class<*>, Int>): ViewTypeManager =
        ViewTypeManagerImpl(provider, viewTypes)

fun viewTypeManagerOf(provider: Provider<*>, vararg viewTypes: Pair<Class<*>, Int>): ViewTypeManager =
        viewTypeManagerOf(provider, mapOf(*viewTypes))

infix fun <T, D : BindingDelegate<T, *, *>> DelegatedAdapter<T>.add(delegate: D): Pair<Class<*>, Int> =
        delegate.type to this + delegate