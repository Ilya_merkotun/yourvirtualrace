package app.virtualrace.core.ui

interface StatusBarStyleable {
    val isLightStatusBar: Boolean get() = false
}