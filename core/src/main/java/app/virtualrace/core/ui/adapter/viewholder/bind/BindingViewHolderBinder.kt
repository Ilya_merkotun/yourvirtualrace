package app.virtualrace.core.ui.adapter.viewholder.bind

import androidx.recyclerview.widget.RecyclerView

interface BindingViewHolderBinder<T, V> : ViewHolderBinder where V : RecyclerView.ViewHolder {
    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        bindViewHolder(holder as? V ?: return, position)
    }

    fun bindViewHolder(holder: V, position: Int) = Unit
}