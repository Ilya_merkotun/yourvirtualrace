package app.virtualrace.core.ui.fragment.overlap

import androidx.fragment.app.Fragment

interface OverlappingParent {
    fun setOverlapTop(overlap: OverlappingChild.Overlap)
}

val Fragment.overlappingParent: OverlappingParent?
    get() {
        var parentFragment: Fragment? = parentFragment
        var parent: OverlappingParent? = null
        while (parent == null && parentFragment != null) {
            parent = parentFragment as? OverlappingParent
            if (parent == null) {
                parentFragment = parentFragment.parentFragment
            }
        }
        return parent
    }