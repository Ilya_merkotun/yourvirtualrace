package app.virtualrace.core.ui.adapter.viewholder

interface ViewTypeManager {
    fun getItemViewType(position: Int, count: Int): Int = DEFAULT_VIEW_TYPE

    companion object {
        const val DEFAULT_VIEW_TYPE = 0
    }
}