package app.virtualrace.core.ui

import app.virtualrace.core.presentation.StateView
import app.virtualrace.core.widget.StateLayout

interface HoldingStateLayout : StateView {
    val stateLayout: StateLayout

    override fun showState(state: StateView.State) = when (state) {
        is StateView.State.Custom -> stateLayout.setCustomState(state.name)
        else -> stateLayout.setState(state.state)
    }
}