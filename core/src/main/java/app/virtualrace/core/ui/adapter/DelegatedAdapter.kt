package app.virtualrace.core.ui.adapter

import app.virtualrace.core.presentation.provider.Provider
import app.virtualrace.core.ui.adapter.delegate.AdapterDelegate
import app.virtualrace.core.ui.adapter.viewholder.ViewHolderCreator
import app.virtualrace.core.ui.adapter.viewholder.ViewTypeManager
import app.virtualrace.core.ui.adapter.viewholder.bind.ViewHolderBinder

interface DelegatedAdapter<T> {
    var provider: Provider<T>
    var viewTypeManager: ViewTypeManager?

    fun registerCreator(creator: ViewHolderCreator): Int
    fun registerBinder(viewType: Int, binder: ViewHolderBinder?)

    operator fun plusAssign(viewTypeManager: ViewTypeManager?)
    operator fun plusAssign(provider: Provider<T>)
    operator fun plusAssign(pair: Pair<ViewHolderCreator, ViewHolderBinder?>)
    operator fun plus(pair: Pair<ViewHolderCreator, ViewHolderBinder?>): Int
    operator fun plus(creator: ViewHolderCreator): Int
    operator fun plus(delegate: AdapterDelegate): Int
}