package app.virtualrace.core.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.virtualrace.core.R
import app.virtualrace.core.presentation.provider.Provider
import app.virtualrace.core.ui.adapter.DelegatedRecyclerAdapter

abstract class BaseListFragment<T> : BaseFragment(), Provider.Delegate {
    protected open val recyclerViewId: Int get() = R.id.recycler_view
    protected open lateinit var recyclerView: RecyclerView

    protected lateinit var adapter: DelegatedRecyclerAdapter<T>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = onCreateAdapter().also { onAdapterCreated(it) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view as? RecyclerView ?: view.findViewById(recyclerViewId)

        onRecyclerViewCreated(requireContext(), recyclerView)
    }

    protected open fun onCreateAdapter(): DelegatedRecyclerAdapter<T> = DelegatedRecyclerAdapter()
    protected open fun onAdapterCreated(adapter: DelegatedRecyclerAdapter<T>) = Unit

    protected open fun onRecyclerViewCreated(context: Context, recyclerView: RecyclerView) {
        val layoutManager = onCreateLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    protected open fun onCreateLayoutManager(context: Context): RecyclerView.LayoutManager =
            LinearLayoutManager(context)

    // region Provider.Delegate
    override fun notifyDataSetChanged() = adapter.notifyDataSetChanged()

    override fun notifyItemChanged(position: Int) = adapter.notifyItemChanged(position)

    override fun notifyItemRangeChanged(positionStart: Int, itemCount: Int) {
        adapter.notifyItemRangeChanged(positionStart, itemCount)
    }

    override fun notifyItemInserted(position: Int) = adapter.notifyItemInserted(position)

    override fun notifyItemMoved(fromPosition: Int, toPosition: Int) {
        adapter.notifyItemMoved(fromPosition, toPosition)
    }

    override fun notifyItemRangeInserted(positionStart: Int, itemCount: Int) {
        adapter.notifyItemRangeInserted(positionStart, itemCount)
    }

    override fun notifyItemRemoved(position: Int) = adapter.notifyItemRemoved(position)

    override fun notifyItemRangeRemoved(positionStart: Int, itemCount: Int) {
        adapter.notifyItemRangeRemoved(positionStart, itemCount)
    }
    // endregion
}