package app.virtualrace.core.ui.fragment.overlap

import android.content.Context
import androidx.annotation.DimenRes

interface OverlappingChild {
    val overlapTop: Overlap get() = Overlap.Offset(0)

    sealed class Overlap {
        object FitSystemWindow : Overlap()
        class Offset(val value: Int = 0) : Overlap() {
            constructor(context: Context, @DimenRes resId: Int) : this(
                context.resources.getDimensionPixelOffset(resId)
            )
        }
    }
}