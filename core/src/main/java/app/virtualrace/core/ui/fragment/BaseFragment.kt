package app.virtualrace.core.ui.fragment

import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import app.virtualrace.core.common.ManagingActiveStatus
import app.virtualrace.core.extension.isVisible
import app.virtualrace.core.presentation.Attachable
import app.virtualrace.core.presentation.Messageable
import app.virtualrace.core.presentation.duration
import app.virtualrace.core.ui.*
import app.virtualrace.core.ui.fragment.overlap.OverlappingChild
import app.virtualrace.core.ui.fragment.overlap.overlappingParent
import com.google.android.material.snackbar.Snackbar

abstract class BaseFragment : Fragment(), ManagingActiveStatus, OverlappingChild, Attachable,
    Messageable {

    protected abstract val layoutResId: Int

    protected open val toolbarTitleRes = 0
    protected open val toolbarTitle: CharSequence? get() = getTextIfNeeded(toolbarTitleRes)

    protected open val toolbarSubtitleRes = 0
    protected open val toolbarSubtitle: CharSequence? get() = getTextIfNeeded(toolbarSubtitleRes)

    protected open val optionsMenuRes: Int = 0

    protected val supportActivity: AppCompatActivity? get() = activity as? AppCompatActivity

    override var isAttached = false
        protected set

    private fun getTextIfNeeded(@StringRes resId: Int): CharSequence? =
        if (resId != 0) getText(resId) else null

    // region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(optionsMenuRes != 0)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(layoutResId, container, false)

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        if (optionsMenuRes != 0) inflater.inflate(optionsMenuRes, menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isAttached = true
    }

    override fun onDestroyView() {
        isAttached = false
        super.onDestroyView()
    }

    override fun onStart() {
        super.onStart()
        if (isHidden) return

        didBecomeActive()
    }

    override fun onStop() {
        super.onStop()
        if (isHidden) return

        didResignActive()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (hidden) didResignActive()
        else didBecomeActive()
    }
    // endregion

    // region ManagingActiveStatus
    override fun didBecomeActive() {
        val parent = parentFragment
        if (parent is HoldingToolbarTitle) {
            parent.titleTextView.text = toolbarTitle
        } else {
            supportActivity?.supportActionBar?.let(::onActionBarReady)
        }

        if (this is VisibleBottomBar) {
            (parent as? HoldingBottomBar)?.bottomView?.isVisible = isVisibleBottomBar
            parent?.view?.requestLayout()
        }

        if (this is VisibleToolbar) {
            if (this.isVisibleToolbar) (requireActivity() as AppCompatActivity).supportActionBar?.show()
            else (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        }

        val overlappingParent = overlappingParent
        if (overlappingParent != this) {
            overlappingParent?.setOverlapTop(overlapTop)
        }

        requireActivity().window?.decorView?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (this is StatusBarStyleable && isLightStatusBar) {
                    it.systemUiVisibility =
                        it.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                } else {
                    it.systemUiVisibility =
                        it.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
                }
            }
        }
    }

    override fun didResignActive() = Unit
    // endregion

    // region Messageable
    override fun message(text: String, duration: Int, @Messageable.Type type: Int) {
        val d = duration(duration, type)

        when (type) {
            Messageable.SNACKBAR -> Snackbar.make(view ?: return, text, d).show()
            Messageable.TOAST,
            Messageable.UNDEFINED -> Toast.makeText(context ?: return, text, d).show()
        }
    }
    // endregion

    open fun onActionBarReady(bar: ActionBar) {
        bar.title = toolbarTitle
        bar.subtitle = toolbarSubtitle
    }
}