package app.virtualrace.core.ui.adapter.provider

interface Obtainable<T> {
    fun getOrNull(index: Int): T?
    operator fun get(index: Int): T = getOrNull(index)!!
}