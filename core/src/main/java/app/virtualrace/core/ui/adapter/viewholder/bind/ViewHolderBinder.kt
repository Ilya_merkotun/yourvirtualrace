package app.virtualrace.core.ui.adapter.viewholder.bind

import androidx.recyclerview.widget.RecyclerView

interface ViewHolderBinder {
    fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = Unit
}