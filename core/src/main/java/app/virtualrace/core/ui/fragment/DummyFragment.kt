package app.virtualrace.core.ui.fragment

import android.os.Bundle
import android.view.View
import android.widget.TextView
import app.virtualrace.core.R
import app.virtualrace.core.ui.StatusBarStyleable

open class DummyFragment : BaseFragment(), StatusBarStyleable {

    companion object {
        private const val KEY_1 = ".KEY_1"
        fun newInstance(text: String? = null) = DummyFragment().apply {
            arguments = Bundle(1).also {
                it.putString(KEY_1, text)
            }
        }
    }

    override val layoutResId get() = R.layout.fragment_dummy
    override val isLightStatusBar = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.getString(KEY_1)?.let {
            view.findViewById<TextView>(R.id.centerTextView).text = it
        }
    }
}