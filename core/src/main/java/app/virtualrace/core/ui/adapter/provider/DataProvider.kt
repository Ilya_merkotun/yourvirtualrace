package app.virtualrace.core.ui.adapter.provider

interface DataProvider {
    fun getItemCount(): Int
}