package app.virtualrace.core.ui.fragment.container

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import app.virtualrace.core.common.internal.LifecycleAwareNavigationHolder
import app.virtualrace.core.ui.fragment.BaseFragment
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import javax.inject.Inject

abstract class BaseContainerFragment : BaseFragment(), ContainerFragment {
    @Inject
    lateinit var holder: LifecycleAwareNavigationHolder

    override val contentFragment: Fragment? get() = childFragmentManager.findFragmentById(containerId)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        holder.register(this)
        holder.setNavigator(navigator)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)

        val fragment = contentFragment ?: return

        childFragmentManager.beginTransaction().apply {
            if (hidden) hide(fragment) else show(fragment)
        }.commit()
    }

    protected open fun activityBack() = Unit
    protected open fun fragmentTransaction(
            command: Command,
            from: Fragment?,
            to: Fragment?,
            transaction: FragmentTransaction
    ) = Unit

    protected val FragmentManager.isBackStackEmpty: Boolean get() = backStackEntryCount == 0

    protected open val navigator: Navigator by lazy {
        object : SupportAppNavigator(requireActivity(), childFragmentManager, containerId) {
            override fun activityBack() = this@BaseContainerFragment.activityBack()

            override fun setupFragmentTransaction(
                    command: Command,
                    currentFragment: Fragment?,
                    nextFragment: Fragment?,
                    fragmentTransaction: FragmentTransaction
            ) {
                fragmentTransaction.setReorderingAllowed(true)
                fragmentTransaction(command, currentFragment, nextFragment, fragmentTransaction)
            }
        }
    }
}