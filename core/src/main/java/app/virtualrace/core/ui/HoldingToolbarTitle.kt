package app.virtualrace.core.ui

import android.widget.TextView
import com.google.android.material.bottomnavigation.BottomNavigationView

interface HoldingToolbarTitle {
    val titleTextView: TextView
}

interface HoldingBottomBar {
    val bottomView: BottomNavigationView?
}

interface VisibleToolbar {
    val isVisibleToolbar: Boolean
    val isBackAllowed: Boolean get() = true
}

interface VisibleBottomBar {
    val isVisibleBottomBar: Boolean
}