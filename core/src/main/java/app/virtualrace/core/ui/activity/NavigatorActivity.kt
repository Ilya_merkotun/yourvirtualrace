package app.virtualrace.core.ui.activity

import android.os.Bundle
import android.view.MenuItem
import app.virtualrace.core.common.internal.LifecycleAwareNavigationHolder
import app.virtualrace.core.common.internal.OnBackPressedHelperImpl
import app.virtualrace.core.di.module.AndroidxActivityModule
import ru.terrakok.cicerone.Navigator
import toothpick.Scope
import javax.inject.Inject

abstract class NavigatorActivity : ScopeAwareActivity() {

    protected abstract val navigator: Navigator

    @Inject
    lateinit var holder: LifecycleAwareNavigationHolder
    @Inject
    internal lateinit var helper: OnBackPressedHelperImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        holder.register(this)
        holder.setNavigator(navigator)
    }

    override fun onScopeCreated(scope: Scope) {
        super.onScopeCreated(scope)
        scope.installModules(AndroidxActivityModule(this))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            helper.invokedFromToolbar = true
            onBackPressed()
            helper.invokedFromToolbar = false
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}