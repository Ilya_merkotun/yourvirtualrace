package app.virtualrace.core.ui.activity

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import app.virtualrace.core.di.fragment.ScopeAware
import app.virtualrace.core.extension.uniqueName
import toothpick.Scope
import toothpick.Toothpick
import toothpick.smoothie.module.SmoothieActivityModule

abstract class ScopeAwareActivity : AppCompatActivity(), ScopeAware {

    override fun onCreate(savedInstanceState: Bundle?) {
        val scope = createScope()
        onScopeCreated(scope)
        super.onCreate(savedInstanceState)
        Toothpick.inject(this, scope)
    }

    override fun onDestroy() {
        if (isFinishing) Toothpick.closeScope(uniqueName)
        super.onDestroy()
    }

    protected open fun createScope(): Scope = Toothpick.openScopes(application, uniqueName)

    @CallSuper
    override fun onScopeCreated(scope: Scope) {
        scope.installModules(SmoothieActivityModule(this))
    }

}