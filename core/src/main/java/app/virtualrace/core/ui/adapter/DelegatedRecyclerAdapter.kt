package app.virtualrace.core.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.virtualrace.core.presentation.provider.Provider
import app.virtualrace.core.presentation.provider.providerOf
import app.virtualrace.core.ui.adapter.delegate.AdapterDelegate
import app.virtualrace.core.ui.adapter.viewholder.ViewHolderCreator
import app.virtualrace.core.ui.adapter.viewholder.ViewTypeManager
import app.virtualrace.core.ui.adapter.viewholder.bind.ViewHolderBinder
import java.lang.ref.WeakReference

class DelegatedRecyclerAdapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder>, DelegatedAdapter<T> {

    constructor() : super()

    constructor(manager: ViewTypeManager? = null) : super() {
        _viewTypeManager = manager
    }

    constructor(weakManager: WeakReference<ViewTypeManager>? = null) : super() {
        _weakViewTypeManager = weakManager
    }

    override var provider: Provider<T> = providerOf()

    private var _weakViewTypeManager: WeakReference<ViewTypeManager>? = null
    private var _viewTypeManager: ViewTypeManager? = null

    var weakViewTypeManager: WeakReference<ViewTypeManager>?
        get() = _weakViewTypeManager
        set(value) {
            _weakViewTypeManager?.clear()
            _weakViewTypeManager = value
        }
    override var viewTypeManager
        get() = _viewTypeManager ?: _weakViewTypeManager?.get()
        set(value) {
            _viewTypeManager = value
        }

    private val viewHolderCreators = mutableMapOf<Int, ViewHolderCreator>()
    private val viewHolderBinders = mutableMapOf<Int, ViewHolderBinder>()

    @Suppress("MemberVisibilityCanBePrivate")
    override fun registerCreator(creator: ViewHolderCreator): Int {
        val index = ViewTypeManager.DEFAULT_VIEW_TYPE + viewHolderCreators.size
        viewHolderCreators[index] = creator
        return index
    }

    @Suppress("MemberVisibilityCanBePrivate")
    override fun registerBinder(viewType: Int, binder: ViewHolderBinder?) {
        if (binder != null) viewHolderBinders[viewType] = binder
    }

    override operator fun plusAssign(viewTypeManager: ViewTypeManager?) {
        this.viewTypeManager = viewTypeManager
    }

    override operator fun plusAssign(provider: Provider<T>) {
        this.provider = provider
    }

    override operator fun plusAssign(pair: Pair<ViewHolderCreator, ViewHolderBinder?>) {
        this + pair
    }

    override operator fun plus(pair: Pair<ViewHolderCreator, ViewHolderBinder?>): Int {
        val viewType = registerCreator(pair.first)
        registerBinder(viewType, pair.second)

        return viewType
    }

    override operator fun plus(creator: ViewHolderCreator) = registerCreator(creator)

    override operator fun plus(delegate: AdapterDelegate) =
            this + ((delegate as ViewHolderCreator) to (delegate as? ViewHolderBinder))

    override fun getItemCount() = provider.getItemCount()

    override fun getItemViewType(position: Int) =
            viewTypeManager?.getItemViewType(position, itemCount)
                    ?: ViewTypeManager.DEFAULT_VIEW_TYPE

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            viewHolderCreators[viewType]?.createViewHolder(parent)
                    ?: error("View holder creator have not been initialized.")

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        viewHolderBinders[holder.itemViewType]?.onBindViewHolder(holder, position)
    }
}