package app.virtualrace.core.ui.fragment.container

import androidx.fragment.app.Fragment

interface ContainerFragment {
    val containerId: Int
    val contentFragment: Fragment?
}