package app.virtualrace.core.ui.adapter.delegate

import androidx.recyclerview.widget.RecyclerView
import app.virtualrace.core.extension.weak
import app.virtualrace.core.presentation.provider.Provider
import app.virtualrace.core.ui.adapter.DelegatedRecyclerAdapter
import app.virtualrace.core.ui.adapter.viewholder.bind.BindingViewHolderBinder
import app.virtualrace.core.ui.adapter.viewholder.viewTypeManagerOf

abstract class BindingDelegate<T, V, L>(
        val type: Class<*>,
        protected val provider: Provider<*>,
        listener: L? = null
) : AdapterDelegate, BindingViewHolderBinder<T, V> where V : RecyclerView.ViewHolder {
    protected val weakListener = listener?.weak()

    @Suppress("UNCHECKED_CAST")
    override fun bindViewHolder(holder: V, position: Int) =
            bindViewHolder(holder, provider[position] as T, position)

    protected open fun bindViewHolder(holder: V, item: T, position: Int) = Unit
}

fun <T> DelegatedRecyclerAdapter<T>.setDelegates(
        vararg delegates: BindingDelegate<*, *, *>
) {
    this += viewTypeManagerOf(this.provider, *delegates.map {
        it.type to (this + it)
    }.toTypedArray())
}