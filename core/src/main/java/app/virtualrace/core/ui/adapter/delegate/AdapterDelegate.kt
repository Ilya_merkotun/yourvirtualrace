package app.virtualrace.core.ui.adapter.delegate

import app.virtualrace.core.ui.adapter.viewholder.bind.ViewHolderBinder
import app.virtualrace.core.ui.adapter.viewholder.ViewHolderCreator

interface AdapterDelegate : ViewHolderCreator, ViewHolderBinder