package app.virtualrace.core.ui.adapter.viewholder

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import app.virtualrace.core.extension.weakWrapper
import java.lang.ref.WeakReference

@Suppress("MemberVisibilityCanBePrivate")
abstract class ViewHolder<L>(
    parent: ViewGroup,
    protected val weakListener: WeakReference<out L>?,
    @LayoutRes layoutRes: Int,
    clickable: Boolean = false
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
), View.OnClickListener {

    val context: Context get() = itemView.context
    val resources: Resources get() = context.resources

    protected val listener: L? get() = weakListener?.get()

    init {
        if (clickable && weakListener != null) {
            itemView.setOnClickListener(weakWrapper())
        }
    }

    override fun onClick(v: View) {
        val listener = weakListener?.get() ?: return
        if (listener is OnViewHolderClickListener) {
            listener.onViewHolderClick(this, adapterPosition, v.id)
        }
    }

    fun <T : View> findViewById(@IdRes id: Int): T = itemView.findViewById(id)

    interface OnViewHolderClickListener {
        fun onViewHolderClick(holder: RecyclerView.ViewHolder, position: Int, id: Int) = Unit
    }
}