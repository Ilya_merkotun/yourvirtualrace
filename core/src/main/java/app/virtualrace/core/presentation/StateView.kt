package app.virtualrace.core.presentation

import app.virtualrace.core.widget.StateLayout

interface StateView {
    sealed class State(@StateLayout.State val state: Int) {
        object Content : State(StateLayout.State.CONTENT)
        object Progress : State(StateLayout.State.PROGRESS)
        object Empty : State(StateLayout.State.EMPTY)
        object None : State(StateLayout.State.NONE)
        class Custom(val name: String) : State(StateLayout.State.CUSTOM)
    }

    fun showState(state: State)
}