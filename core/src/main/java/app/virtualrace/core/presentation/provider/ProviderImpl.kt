package app.virtualrace.core.presentation.provider

internal open class ProviderImpl<T>(items: Collection<T>?) : AbstractProvider<T>() {
    override var holder = items?.toMutableList() ?: mutableListOf()
}