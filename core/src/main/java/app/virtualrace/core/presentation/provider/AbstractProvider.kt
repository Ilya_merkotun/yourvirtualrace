package app.virtualrace.core.presentation.provider

import app.virtualrace.core.extension.weak
import java.lang.ref.WeakReference

abstract class AbstractProvider<T> : Provider<T>, MutableList<T> {

    protected abstract var holder: MutableList<T>

    private var _delegate: WeakReference<Provider.Delegate>? = null
    override var delegate: Provider.Delegate?
        get() = _delegate?.get()
        set(value) {
            _delegate = value?.weak()
        }

    override val size: Int get() = holder.size

    // region Mutable
    override fun add(element: T): Boolean {
        val index = holder.lastIndex
        holder.add(element)
        delegate?.notifyItemInserted(index)
        return true
    }

    override fun remove(element: T): Boolean {
        val index = holder.indexOf(element)
        val removed = holder.remove(element)
        if (removed && index != -1) delegate?.notifyItemRemoved(index)
        return removed
    }

    override fun addAll(elements: Collection<T>): Boolean {
        val lastIndex = holder.lastIndex
        val changed = holder.addAll(elements)
        if (changed) delegate?.notifyItemRangeInserted(lastIndex, elements.size)
        return changed
    }

    override fun addAll(index: Int, elements: Collection<T>): Boolean {
        val changed = holder.addAll(index, elements)
        if (changed) delegate?.notifyItemRangeInserted(index, elements.size)
        return changed
    }

    override fun removeAll(elements: Collection<T>): Boolean {
        val modified = holder.toMutableList()
        val removed = modified.removeAll(elements)
        holder = modified
        delegate?.notifyDataSetChanged()
        return removed
    }

    override fun retainAll(elements: Collection<T>): Boolean {
        val modified = holder.toMutableList()
        val removed = modified.retainAll(elements)
        holder = modified
        delegate?.notifyDataSetChanged()
        return removed
    }

    override operator fun set(index: Int, element: T): T {
        val previous = holder.set(index, element)
        delegate?.notifyItemChanged(index)
        return previous
    }

    override fun add(index: Int, element: T) {
        holder.add(index, element)
        delegate?.notifyItemInserted(index)
    }

    override fun removeAt(index: Int): T {
        val removed = holder.removeAt(index)
        delegate?.notifyItemRemoved(index)
        return removed
    }

    override fun clear() {
        val count = size
        holder.clear()
        delegate?.notifyItemRangeRemoved(0, count)
    }
    // endregion

    // region List

    override fun contains(element: T): Boolean = holder.contains(element)
    override fun containsAll(elements: Collection<T>): Boolean = holder.containsAll(elements)
    override fun get(index: Int): T = holder[index]
    override fun indexOf(element: T): Int = holder.indexOf(element)
    override fun isEmpty(): Boolean = holder.isEmpty()
    override fun iterator(): MutableIterator<T> = holder.iterator()
    override fun lastIndexOf(element: T): Int = holder.lastIndexOf(element)
    override fun listIterator(): MutableListIterator<T> = holder.listIterator()
    override fun listIterator(index: Int): MutableListIterator<T> = holder.listIterator(index)
    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun subList(from: Int, to: Int): MutableList<T> = holder.subList(from, to)

    // endregion

    override fun set(data: List<T>) {
        val mutable = data as? MutableList<T> ?: data.toMutableList()
        holder = mutable
        delegate?.notifyDataSetChanged()
    }

    override fun toList(): List<T> = holder
}