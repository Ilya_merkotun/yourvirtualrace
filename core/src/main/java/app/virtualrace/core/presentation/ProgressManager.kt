package app.virtualrace.core.presentation

interface ProgressManager {
    fun showProgress()
    fun hideProgress()
}