package app.virtualrace.core.presentation.common

interface Starting {
    fun start()
}