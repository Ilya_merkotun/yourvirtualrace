package app.virtualrace.core.presentation.router

import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppScreen

open class FlowRouter(private val router: Router) : Router() {
    fun startFlow(screen: SupportAppScreen) = router.newRootScreen(screen)
    fun finishFlow() = router.exit()
}