package app.virtualrace.core.presentation.presenter

import app.virtualrace.core.common.ExceptionHumanizer
import app.virtualrace.core.common.Executor
import app.virtualrace.core.common.ResourceManager
import app.virtualrace.core.presentation.Attachable
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class ExceptionHandlingPresenter<V : Attachable>(
        view: V, job: Job, executor: Executor,
        protected val humanizer: ExceptionHumanizer,
        protected val resourceManager: ResourceManager
) : CoroutinePresenter<V>(view, job, executor) {
    abstract val handler: CoroutineExceptionHandler
    override val coroutineContext: CoroutineContext by lazy { super.coroutineContext + handler }
}