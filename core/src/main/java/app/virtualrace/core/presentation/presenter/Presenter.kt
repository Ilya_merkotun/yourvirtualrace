package app.virtualrace.core.presentation.presenter

import app.virtualrace.core.extension.weak

abstract class Presenter<V>(view: V) {
    protected val reference = view.weak()
    protected val view: V?
        get() = reference.get()
}