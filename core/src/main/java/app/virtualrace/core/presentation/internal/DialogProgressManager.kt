package app.virtualrace.core.presentation.internal

import android.app.Activity
import androidx.appcompat.app.AlertDialog
import app.virtualrace.core.R
import app.virtualrace.core.extension.unit
import app.virtualrace.core.extension.weak
import app.virtualrace.core.presentation.ProgressManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.lang.ref.WeakReference
import javax.inject.Inject

class DialogProgressManager(
        activity: Activity,
        private val customizer: Customizer
) : ProgressManager {

    @Inject
    constructor(activity: Activity) : this(activity, Customizer())

    private val weakActivity = activity.weak()
    private var weakDialog: WeakReference<AlertDialog>? = null

    private val activity get() = weakActivity.get()
    private val dialog get() = weakDialog?.get() ?: activity?.let { createDialog(it) }

    private fun createDialog(activity: Activity) = MaterialAlertDialogBuilder(activity)
            .setView(customizer.layoutResId)
            .setCancelable(false)
            .create().apply {
                customizer.customize(this)

                weakDialog = weak()
            }

    override fun showProgress() = dialog?.show().unit()

    override fun hideProgress() {
        weakDialog?.get()?.dismiss()
        weakDialog = null
    }

    open class Customizer(val layoutResId: Int = R.layout.dialog_progress) {
        open fun customize(dialog: AlertDialog) = Unit
    }
}