package app.virtualrace.core.presentation

interface Refreshable : Attachable {
    var isRefreshing: Boolean
}