package app.virtualrace.core.presentation

interface Attachable {
    val isAttached: Boolean
}