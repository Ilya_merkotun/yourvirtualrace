package app.virtualrace.core.presentation.presenter

import app.virtualrace.core.common.Executor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class CoroutinePresenter<V>(
        view: V,
        protected val job: Job,
        protected val executor: Executor
) : Presenter<V>(view), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = executor.ui + job
}