@file:Suppress("unused")

package app.virtualrace.core.presentation.provider

import app.virtualrace.core.ui.adapter.provider.DataProvider

interface Provider<T> : MutableList<T>, DataProvider {

    interface Delegate {
        fun notifyDataSetChanged()
        fun notifyItemChanged(position: Int) = notifyDataSetChanged()
        fun notifyItemRangeChanged(positionStart: Int, itemCount: Int) = notifyDataSetChanged()
        fun notifyItemInserted(position: Int) = notifyDataSetChanged()
        fun notifyItemMoved(fromPosition: Int, toPosition: Int) = notifyDataSetChanged()
        fun notifyItemRangeInserted(positionStart: Int, itemCount: Int) = notifyDataSetChanged()
        fun notifyItemRemoved(position: Int) = notifyDataSetChanged()
        fun notifyItemRangeRemoved(positionStart: Int, itemCount: Int) = notifyDataSetChanged()
    }

    override fun getItemCount() = size

    var delegate: Delegate?
    fun toList(): List<T>
    fun set(data: List<T>)
}

fun <T> providerOf(collection: Collection<T>? = null): Provider<T> = ProviderImpl(collection)
fun <T> providerOf(vararg items: T): Provider<T> = ProviderImpl(mutableListOf(*items))