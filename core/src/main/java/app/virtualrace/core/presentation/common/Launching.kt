package app.virtualrace.core.presentation.common

interface Launching {
    fun launch()
}