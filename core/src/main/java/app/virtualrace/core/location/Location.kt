package app.virtualrace.core.location

import android.os.Parcel
import android.os.Parcelable

typealias GPSLocation = Location

data class Location(
        val latitude: Double,
        val longitude: Double,
        val altitude: Double = 0.0,
        val speed: Float = 0f
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readDouble(), parcel.readDouble(), parcel.readDouble(), parcel.readFloat())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeDouble(altitude)
        parcel.writeFloat(speed)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<Location> {
        override fun createFromParcel(parcel: Parcel): Location {
            return Location(parcel)
        }

        override fun newArray(size: Int): Array<Location?> {
            return arrayOfNulls(size)
        }
    }
}