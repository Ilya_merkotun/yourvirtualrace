package app.virtualrace.core.location

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.gms.location.LocationAvailability
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import app.virtualrace.core.extension.unit
import app.virtualrace.core.extension.weak
import java.lang.ref.WeakReference

typealias AndroidLocation = android.location.Location
typealias AndroidLocationCallback = com.google.android.gms.location.LocationCallback

@Suppress("unused")
class AndroidLocationManager(context: Context,
                             private val interval: Long,
                             private val fastestInterval: Long,
                             private val priority: Int
) : AndroidLocationCallback(), LocationManager,
        LifecycleObserver {
    private val weakContext: WeakReference<Context> = context.weak()

    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

    private val locationUpdatesListeners: MutableList<WeakReference<LocationUpdatesListener>> =
            mutableListOf()

    private var weakLastKnownLocationCallback: WeakReference<LastKnownLocationCallback>? = null

    constructor(
            lifecycleOwner: LifecycleOwner,
            context: Context,
            interval: Long,
            fastestInterval: Long,
            priority: Int
    ) : this(context, interval, fastestInterval, priority) {
        lifecycleOwner.lifecycle.addObserver(this)
    }

    constructor(
            fragment: Fragment,
            interval: Long,
            fastestInterval: Long,
            priority: Int
    ) : this(fragment, fragment.requireContext(), interval, fastestInterval, priority)

    constructor(
            activity: FragmentActivity,
            interval: Long,
            fastestInterval: Long,
            priority: Int
    ) : this(activity, activity, interval, fastestInterval, priority)

    private val isPermissionsGranted: Boolean
        get() = weakContext.get()?.let {
            ActivityCompat.checkSelfPermission(it, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(it, ACCESS_COARSE_LOCATION) == PERMISSION_GRANTED
        } ?: false

    // region LocationManager

    @SuppressLint("MissingPermission")
    override fun getLastKnownLocation(callback: LastKnownLocationCallback) {
        weakLastKnownLocationCallback = callback.weak()

        if (isPermissionsGranted) {
            fusedLocationClient.lastLocation.addOnCompleteListener { task ->
                val lastKnownLocationCallback =
                        weakLastKnownLocationCallback?.get() ?: return@addOnCompleteListener

                val location = task.result?.location
                if (location == null) {
                    lastKnownLocationCallback.onLastKnownLocationFailure()
                } else {
                    lastKnownLocationCallback.onLastKnownLocation(location)
                }
            }

            fusedLocationClient.lastLocation.addOnFailureListener {
                weakLastKnownLocationCallback?.get()?.onLastKnownLocationFailure()
            }
        }
    }

    override fun addLocationUpdatesListener(listener: LocationUpdatesListener) {
        if (locationUpdatesListeners.firstOrNull { it.get() == listener } == null) {
            locationUpdatesListeners.add(listener.weak())
        }
    }

    override fun removeLocationUpdatesListener(listener: LocationUpdatesListener) {
        locationUpdatesListeners.firstOrNull { it.get() == listener }?.let {
            locationUpdatesListeners.remove(it)
        }
    }

    // endregion

    // region LifecycleObserver

    @SuppressLint("MissingPermission")
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        if (isPermissionsGranted) {
            val context = weakContext.get() ?: return
            val request = LocationRequest().apply {
                interval = this@AndroidLocationManager.interval
                fastestInterval = this@AndroidLocationManager.fastestInterval
                priority = this@AndroidLocationManager.priority
            }

            val requestSecond = LocationRequest().apply {
                fastestInterval = 0
                priority = this@AndroidLocationManager.priority
                smallestDisplacement = 0f
            }

            fusedLocationClient.requestLocationUpdates(request, this, context.mainLooper)
            fusedLocationClient.requestLocationUpdates(requestSecond, this, context.mainLooper)  // temp solution for client  / request updates every 1 sec
        }

        locationUpdatesListeners.forEach {
            if (it.get() == null) locationUpdatesListeners.remove(it)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() = fusedLocationClient.removeLocationUpdates(this).unit()

    // endregion

    // region AndroidLocationCallback

    override fun onLocationResult(locationResult: LocationResult?) {
        val lastLocation = locationResult?.lastLocation?.location
        val locations = locationResult?.locations?.map { it.location } ?: return

        locationUpdatesListeners.forEach { it ->
            val listener = it.get() ?: return@forEach

            listener.onLocationsUpdated(locations)

            lastLocation?.let { listener.onLastLocationUpdated(it) }
        }
    }


    override fun onLocationAvailability(locationAvailability: LocationAvailability?) = Unit

    // endregion
}

private val AndroidLocation.location
    get() = Location(
            this.latitude,
            this.longitude,
            this.altitude
    )