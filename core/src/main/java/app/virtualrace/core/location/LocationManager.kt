package app.virtualrace.core.location

interface LocationManager {
    fun getLastKnownLocation(callback: LastKnownLocationCallback)
    fun addLocationUpdatesListener(listener: LocationUpdatesListener)
    fun removeLocationUpdatesListener(listener: LocationUpdatesListener)
}

interface LastKnownLocationCallback {
    fun onLastKnownLocation(location: Location)
    fun onLastKnownLocationFailure() = Unit
}

interface LocationUpdatesListener {
    fun onLocationsUpdated(locations: List<Location>) = Unit
    fun onLastLocationUpdated(location: Location) = Unit
}