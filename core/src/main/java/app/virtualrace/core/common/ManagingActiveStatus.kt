package app.virtualrace.core.common

interface ManagingActiveStatus {
    fun didBecomeActive()
    fun didResignActive()
}