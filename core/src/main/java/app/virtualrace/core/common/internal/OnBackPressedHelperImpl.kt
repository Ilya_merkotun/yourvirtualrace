package app.virtualrace.core.common.internal

import app.virtualrace.core.common.OnBackPressedHelper

internal class OnBackPressedHelperImpl : OnBackPressedHelper {
    override var invokedFromToolbar: Boolean = false
}