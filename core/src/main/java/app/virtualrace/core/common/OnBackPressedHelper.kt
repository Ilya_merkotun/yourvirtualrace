package app.virtualrace.core.common

interface OnBackPressedHelper {
    val invokedFromToolbar: Boolean
}