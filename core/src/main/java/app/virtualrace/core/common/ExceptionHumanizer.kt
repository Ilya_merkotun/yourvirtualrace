package app.virtualrace.core.common

interface ExceptionHumanizer {
    fun humanize(throwable: Throwable): String
}