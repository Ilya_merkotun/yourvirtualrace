package app.virtualrace.core.common.internal

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import app.virtualrace.core.common.PermissionManager
import app.virtualrace.core.common.PermissionsCallback
import app.virtualrace.core.extension.unit
import java.lang.ref.WeakReference

/**
 * Need to add following code to [AppCompatActivity.onActivityResult]:
 * ```
 * val delegate = ActivityCompat.getPermissionCompatDelegate()
 * if (delegate is ActivityCompat.OnRequestPermissionsResultCallback) {
 *     delegate.onRequestPermissionsResult(requestCode, permissions, grantResults)
 * }
 * ```
 */
class AndroidPermissionManager(
        activity: AppCompatActivity,
        vararg permissions: String
) : PermissionManager, LifecycleObserver,
        ActivityCompat.PermissionCompatDelegate,
        ActivityCompat.OnRequestPermissionsResultCallback {

    companion object {
        private const val REQUEST_CODE = 0xABC
    }

    private val permissions: Array<out String>

    private var reference: WeakReference<AppCompatActivity>? = null
    private var weakCallback: WeakReference<PermissionsCallback>? = null
    private val callback: PermissionsCallback? get() = weakCallback?.get()

    constructor(
            fragment: Fragment,
            vararg permissions: String
    ) : this(fragment.activity as AppCompatActivity, *permissions)

    init {
        reference = WeakReference(activity)
        this.permissions = permissions
        activity.lifecycle.addObserver(this)
        ActivityCompat.setPermissionCompatDelegate(this)
    }

    override val granted: Boolean get() = granted(*permissions)

    override fun granted(vararg permissions: String): Boolean = reference?.get()?.let { ctx ->
        permissions.all { permission ->
            ActivityCompat.checkSelfPermission(ctx, permission) == PackageManager.PERMISSION_GRANTED
        }
    } == true

    override fun request(callback: PermissionsCallback, vararg permissions: String) {
        val activity = reference?.get() ?: return
        weakCallback = WeakReference(callback)
        ActivityCompat.requestPermissions(activity, permissions, REQUEST_CODE)
    }

    override fun request(callback: PermissionsCallback) {
        val activity = reference?.get() ?: return
        weakCallback = WeakReference(callback)
        ActivityCompat.requestPermissions(activity, permissions, REQUEST_CODE)
    }

    override fun requestPermissions(
            activity: Activity, permissions: Array<out String>, requestCode: Int
    ): Boolean = false

    override fun onActivityResult(
            activity: Activity, requestCode: Int, resultCode: Int, data: Intent?
    ): Boolean = handle(requestCode, resultCode)

    override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) = handle(requestCode, Activity.RESULT_OK).unit()

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun release() {
        reference = null
        weakCallback = null
        ActivityCompat.setPermissionCompatDelegate(null)
    }

    private fun handle(requestCode: Int, resultCode: Int): Boolean =
            if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                callback?.onPermissionsGranted(granted)
                weakCallback = null
                true

            } else false
}