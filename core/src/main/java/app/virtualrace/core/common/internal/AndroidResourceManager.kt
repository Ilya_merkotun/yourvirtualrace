package app.virtualrace.core.common.internal

import android.content.res.Resources
import app.virtualrace.core.common.ResourceManager

open class AndroidResourceManager(private val resources: Resources) : ResourceManager {
    override fun getString(resId: Int, vararg args: Any): String =
            resources.getString(resId, *args)

    override fun getQuantityString(resId: Int, quantity: Int, vararg args: Any): String =
            resources.getQuantityString(resId, quantity, *args)
}