package app.virtualrace.core.common

import app.virtualrace.core.extension.weak
import java.util.Collections.singleton

interface WeakCollection<T> : Collection<T> {
    fun add(item: T)
    fun remove(item: T): T?
}

internal class WeakCollectionImpl<T>(
        items: Collection<T>
) : WeakCollection<T> {
    private val weakList = items.map { it.weak() }.toMutableList()

    private val holder: Collection<T>
        get() = weakList.flatMap { singleton<T>(it.get() ?: return@flatMap emptyList<T>()) }

    override val size get() = weakList.size

    override fun contains(element: T) = holder.contains(element)

    override fun containsAll(elements: Collection<T>) = holder.containsAll(elements)

    override fun isEmpty() = weakList.isEmpty()

    override fun iterator() = holder.iterator()

    override fun add(item: T) {
        weakList.add(item.weak())
    }

    override fun remove(item: T): T? = weakList.firstOrNull { it.get() == item }?.let {
        weakList.remove(it)
        it.get()
    }
}

fun <T> weakCollectionOf(vararg items: T): WeakCollection<T> = WeakCollectionImpl(items.asList())