package app.virtualrace.core.common.internal

import app.virtualrace.core.common.Executor
import kotlinx.coroutines.Dispatchers

class AndroidExecutor : Executor {
    override val io = Dispatchers.IO
    override val ui = Dispatchers.Main
    override val network = Dispatchers.Default
}