package app.virtualrace.core.common

interface PermissionManager {
    val granted: Boolean

    fun granted(vararg permissions: String): Boolean
    fun request(callback: PermissionsCallback, vararg permissions: String)
    fun request(callback: PermissionsCallback)
}

interface PermissionsCallback {
    fun onPermissionsGranted(granted: Boolean)
}