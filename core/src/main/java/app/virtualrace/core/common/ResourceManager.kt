package app.virtualrace.core.common

interface ResourceManager {
    fun getString(resId: Int, vararg args: Any): String
    fun getQuantityString(resId: Int, quantity: Int, vararg args: Any): String
}