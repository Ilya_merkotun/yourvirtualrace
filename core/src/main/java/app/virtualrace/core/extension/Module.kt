package app.virtualrace.core.extension

import androidx.lifecycle.LifecycleOwner
import app.virtualrace.core.common.internal.AndroidJob
import kotlinx.coroutines.Job
import toothpick.config.Module

@Suppress("unused")
fun Module.androidJob(owner: LifecycleOwner): Job = AndroidJob(owner.lifecycle)