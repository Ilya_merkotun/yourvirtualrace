package app.virtualrace.core.extension

import android.app.Activity
import android.view.inputmethod.InputMethodManager

fun Activity.hideSoftInput() = currentFocus?.windowToken?.let {
    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(it, 0)
}.unit()