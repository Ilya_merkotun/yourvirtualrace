package app.virtualrace.core.extension

import android.view.Gravity
import android.view.View
import androidx.annotation.IntDef

var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) = changeVisibility(value)

fun View.changeVisibility(visible: Boolean, invisibleState: Int = View.GONE) {
    visibility = if (visible) View.VISIBLE else invisibleState
}

fun View.OnClickListener.weakWrapper() = weak().let { weak ->
    View.OnClickListener {
        weak.get()?.onClick(it)
    }
}

@IntDef(Gravity.START, Gravity.TOP, Gravity.END, Gravity.BOTTOM)
annotation class PaddingFlag

fun View.setPadding(
        padding: Int,
        @PaddingFlag
        flag: Int
) = when (flag) {
    Gravity.START -> setPadding(padding, paddingTop, paddingRight, paddingBottom)
    Gravity.TOP -> setPadding(paddingLeft, padding, paddingRight, paddingBottom)
    Gravity.END -> setPadding(paddingLeft, paddingTop, padding, paddingBottom)
    Gravity.BOTTOM -> setPadding(paddingLeft, paddingTop, paddingRight, padding)
    else -> Unit
}