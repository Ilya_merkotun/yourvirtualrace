package app.virtualrace.core.extension

import android.content.Context
import android.widget.Toast

fun Context.notImplemented() = Toast.makeText(this, "not implemented yet", Toast.LENGTH_SHORT).show()