@file:Suppress("unused")

package app.virtualrace.core.extension

import java.lang.ref.WeakReference

fun <T> T.weak() = WeakReference(this)
fun Any?.unit() = Unit
fun Boolean.toInt() = if (this) 1 else 0


val Any.className: String get() = javaClass.simpleName
val Any.uniqueName: String get() = this::class.java.simpleName + "@" + hashCode()