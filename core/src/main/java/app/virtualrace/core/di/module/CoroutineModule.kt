package app.virtualrace.core.di.module

import androidx.lifecycle.LifecycleOwner
import app.virtualrace.core.extension.androidJob
import kotlinx.coroutines.Job
import toothpick.config.Module

@Suppress("LeakingThis")
abstract class CoroutineModule(owner: LifecycleOwner) : Module() {
    init {
        bind(Job::class.java).toInstance(androidJob(owner))
    }
}