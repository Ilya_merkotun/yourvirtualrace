package app.virtualrace.core.di.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import app.virtualrace.core.extension.uniqueName
import toothpick.Scope
import toothpick.Toothpick

object ScopeAwareLifecycleCallbacks : FragmentManager.FragmentLifecycleCallbacks() {
    override fun onFragmentPreCreated(fm: FragmentManager, f: Fragment, savedInstanceState: Bundle?) {
        if (f is ScopeAware) {
            val scope = f.scope

            f.onScopeCreated(scope)

            Toothpick.inject(f, scope)
        }
    }

    override fun onFragmentDestroyed(fm: FragmentManager, f: Fragment) {
        super.onFragmentDestroyed(fm, f)
        if (f is ScopeAware) {
            Toothpick.closeScope(uniqueName)
        }
    }

    private val Fragment.scope: Scope
        get() {
            val parent = requireNotNull(parentFragment ?: activity).uniqueName
            return Toothpick.openScopes(parent, uniqueName)
        }
}