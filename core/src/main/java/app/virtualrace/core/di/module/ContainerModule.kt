package app.virtualrace.core.di.module

import app.virtualrace.core.common.internal.LifecycleAwareNavigationHolder
import app.virtualrace.core.presentation.router.FlowRouter
import ru.terrakok.cicerone.Cicerone
import toothpick.config.Module

open class ContainerModule<R : FlowRouter>(router: R, clazz: Class<R>) : Module() {
    init {
        Cicerone.create(router).also {
            bind(clazz).toInstance(it.router)
            bind(LifecycleAwareNavigationHolder::class.java)
                .toInstance(LifecycleAwareNavigationHolder(it.navigatorHolder))
        }
    }
}