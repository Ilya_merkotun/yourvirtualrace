package app.virtualrace.core.di.fragment

import toothpick.Scope

interface ScopeAware {
    fun onScopeCreated(scope: Scope) = Unit
}