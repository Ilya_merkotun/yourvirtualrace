package app.virtualrace.core.view

import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import app.virtualrace.core.extension.setPadding

object FittingSystemWindowCallback : FragmentManager.FragmentLifecycleCallbacks() {
    override fun onFragmentViewCreated(fm: FragmentManager, f: Fragment, v: View, savedInstanceState: Bundle?) {
        super.onFragmentViewCreated(fm, f, v, savedInstanceState)

        if (f is FittingSystemWindow && f.fitsSystemWindows) {
            v.fitsSystemWindows = true

            if (f is WindowInsetsGravityAware) {
                ViewCompat.setOnApplyWindowInsetsListener(v) { view, insets ->
                    f.onApplyWindowInsets(view, insets).also {
                        view.setPadding(it.systemWindowInsetTop, f.windowInsetsGravity)
                    }
                }
            } else {
                ViewCompat.setOnApplyWindowInsetsListener(v, f)
            }

            ViewCompat.requestApplyInsets(v)
        } else {
            v.fitsSystemWindows = false
        }
    }
}