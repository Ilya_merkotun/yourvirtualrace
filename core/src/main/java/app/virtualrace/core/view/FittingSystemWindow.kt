package app.virtualrace.core.view

import android.view.Gravity
import android.view.View
import androidx.core.view.OnApplyWindowInsetsListener
import androidx.core.view.WindowInsetsCompat

interface FittingSystemWindow : OnApplyWindowInsetsListener {
    val fitsSystemWindows: Boolean get() = true

    override fun onApplyWindowInsets(v: View, insets: WindowInsetsCompat) = insets
}

interface WindowInsetsGravityAware {
    val windowInsetsGravity: Int get() = Gravity.NO_GRAVITY
}