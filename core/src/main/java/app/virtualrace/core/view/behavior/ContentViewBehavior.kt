package app.virtualrace.core.view.behavior

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.View.MeasureSpec
import androidx.coordinatorlayout.widget.CoordinatorLayout
import app.virtualrace.core.R
import app.virtualrace.core.ui.fragment.overlap.OverlappingChild

class ContentViewBehavior(
    context: Context, attrs: AttributeSet?
) : CoordinatorLayout.Behavior<View>(context, attrs) {

    var overlapTop: OverlappingChild.Overlap = OverlappingChild.Overlap.Offset()

    var topAnchorId: Int = 0
    var bottomAnchorId: Int = 0

    private val dependedViewIds: Set<Int>

    init {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.CoordinatorLayout_LayoutParams)
        topAnchorId =
            ta.getResourceId(R.styleable.CoordinatorLayout_LayoutParams_layout_topAnchorId, 0)
        bottomAnchorId =
            ta.getResourceId(R.styleable.CoordinatorLayout_LayoutParams_layout_bottomAnchorId, 0)

        dependedViewIds = setOf(topAnchorId, bottomAnchorId)

        ta.recycle()
    }

    override fun layoutDependsOn(
        parent: CoordinatorLayout,
        child: View,
        dependency: View
    ): Boolean {
        return dependedViewIds.contains(dependency.id)
    }

    override fun onDependentViewChanged(
        parent: CoordinatorLayout,
        child: View,
        dependency: View
    ): Boolean {
        var modified = false

        var topOffset = 0
        var bottomOffset = 0

        when (val overlapTop = overlapTop) {
            is OverlappingChild.Overlap.Offset -> {
                if (dependency.id == topAnchorId) {
                    val translationY = dependency.height.toFloat() - overlapTop.value

                    if (translationY != child.translationY) {
                        child.translationY = translationY

                        topOffset = translationY.toInt()

                        modified = true
                    }
                }
            }
            is OverlappingChild.Overlap.FitSystemWindow -> {
                if (child.translationY != 0f) {
                    child.translationY = 0f

                    modified = true
                }
            }
        }

        if (dependency.id == bottomAnchorId) {
            bottomOffset = dependency.height
        }

        val requiredHeight = parent.height - topOffset - bottomOffset

        if (requiredHeight != child.layoutParams.height) {
            modified = true
        }

        if (modified) {
            parent.requestLayout()
        }

        return modified
    }

    override fun onLayoutChild(
        parent: CoordinatorLayout,
        child: View,
        layoutDirection: Int
    ): Boolean {
        val dependencies = parent.getDependencies(child)

        val topView = dependencies.firstOrNull { it.id == topAnchorId }

        if (topView != null) {
            parent.onLayoutChild(child, layoutDirection)

            when (overlapTop) {
                is OverlappingChild.Overlap.FitSystemWindow -> {
                    topView.bringToFront()
                }
                is OverlappingChild.Overlap.Offset -> {
                    child.bringToFront()
                }
            }

            return onDependentViewChanged(parent, child, topView)
        }

        return super.onLayoutChild(parent, child, layoutDirection)
    }

    override fun onMeasureChild(
        parent: CoordinatorLayout,
        child: View,
        parentWidthMeasureSpec: Int,
        widthUsed: Int,
        parentHeightMeasureSpec: Int,
        heightUsed: Int
    ): Boolean {
        val dependencies = parent.getDependencies(child)

        val topView = dependencies.firstOrNull { it.id == topAnchorId }
        val bottomView = dependencies.firstOrNull { it.id == bottomAnchorId }

        var topOffset = 0
        val bottomOffset = bottomView?.height ?: 0

        when (val overlapTop = overlapTop) {
            is OverlappingChild.Overlap.Offset -> {
                topOffset = (topView?.height ?: 0) - overlapTop.value
            }
        }

        val requiredHeight = parent.height - topOffset - bottomOffset

        var heightMode = MeasureSpec.getMode(parentHeightMeasureSpec)
        var height = MeasureSpec.getSize(parentHeightMeasureSpec)

        if (heightMode != MeasureSpec.EXACTLY || height != requiredHeight) {
            height = requiredHeight
            heightMode = MeasureSpec.EXACTLY

            val heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, heightMode)

            parent.onMeasureChild(
                child,
                parentWidthMeasureSpec,
                widthUsed,
                heightMeasureSpec,
                heightUsed
            )

            return true
        }

        return false
    }
}