@file:Suppress("unused")

package app.virtualrace.core.view.behavior

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.isVisible
import com.google.android.material.snackbar.Snackbar
import app.virtualrace.core.R

class BottomNavigationViewBehavior<V : View>(
        context: Context,
        attrs: AttributeSet?
) : CoordinatorLayout.Behavior<V>(context, attrs), CoordinatorLayout.AttachedBehavior {
    private val spacing = context.resources.getDimensionPixelOffset(R.dimen.spacing)

    override fun getBehavior(): CoordinatorLayout.Behavior<*> = this

    override fun layoutDependsOn(parent: CoordinatorLayout, child: V, dependency: View): Boolean {
        val layoutParams = dependency.layoutParams
        if (layoutParams is CoordinatorLayout.LayoutParams) {
            val offset = if (child.isVisible) child.height else 0

            if (dependency is Snackbar.SnackbarLayout) {
                layoutParams.bottomMargin = offset + spacing

            }

            dependency.layoutParams = layoutParams
        }
        return super.layoutDependsOn(parent, child, dependency)
    }
}