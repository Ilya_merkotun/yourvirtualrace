package app.virtualrace.core.widget.recycler

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.View
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.roundToInt

class DividerItemDecoration(context: Context) : RecyclerView.ItemDecoration() {
    companion object {
        private val ATTRIBUTES = intArrayOf(android.R.attr.listDivider)
    }

    private var divider: Drawable? = null
    private val bounds = Rect()

    var lookup: ItemDecorationLookup? = null

    init {
        context.obtainStyledAttributes(ATTRIBUTES).apply {
            divider = getDrawable(0)
            recycle()
        }
    }

    fun setDivider(divider: Drawable?) {
        this.divider = divider
    }

    fun setDivider(context: Context, @DrawableRes resId: Int) {
        setDivider(ContextCompat.getDrawable(context, resId))
    }

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.layoutManager !is LinearLayoutManager) return

        val divider = divider ?: return

        canvas.save()

        val left: Int
        val right: Int


        if (parent.clipToPadding) {
            left = parent.paddingLeft
            right = parent.width - parent.paddingRight

            canvas.clipRect(left, parent.paddingTop, right, parent.height - parent.paddingBottom)
        } else {
            left = 0
            right = parent.width
        }

        val childCount = parent.childCount

        var child: View

        var top: Int
        var bottom: Int

        var adapterPosition: Int

        for (position in 0 until childCount) {
            child = parent.getChildAt(position)

            adapterPosition = parent.getChildAdapterPosition(child)

            parent.getDecoratedBoundsWithMargins(child, bounds)

            if (lookup?.drawDividerForPosition(adapterPosition) == false) continue

            bottom = bounds.bottom + child.translationY.roundToInt()
            top = bottom - divider.intrinsicHeight

            divider.setBounds(left, top, right, bottom)
            divider.draw(canvas)
        }

        canvas.restore()
    }
}