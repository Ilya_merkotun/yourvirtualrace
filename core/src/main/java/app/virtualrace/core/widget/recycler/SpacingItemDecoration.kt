package app.virtualrace.core.widget.recycler

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class SpacingItemDecoration(
        private val spacing: Int
) : RecyclerView.ItemDecoration() {

    private val halfSpacing = spacing / 2

    constructor(context: Context, @DimenRes spacingResId: Int) :
            this(context.resources.getDimensionPixelOffset(spacingResId))

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view)

        when (val manager = parent.layoutManager ?: return) {
            is GridLayoutManager -> getGridItemOffsets(position, manager, outRect)
            is LinearLayoutManager -> getListItemOffsets(position, outRect, parent)
        }

    }

    private fun getGridItemOffsets(position: Int, manager: GridLayoutManager, outRect: Rect) {
        val spanSize = manager.spanSizeLookup.getSpanSize(position)

        val spanCount = manager.spanCount


        val groupIndex = manager.spanSizeLookup.getSpanGroupIndex(position, spanCount)
        val index = manager.spanSizeLookup.getSpanIndex(position, spanCount)

        val column = index % spanCount

        if (spanSize == spanCount) return

        if (groupIndex == 0 || manager.spanSizeLookup.getSpanSize(position - index - 1) == spanCount) {
            outRect.top = spacing
        }

        outRect.left = spacing - column * spacing / spanCount
        outRect.right = (column + 1) * spacing / spanCount
        outRect.bottom = spacing
    }

    private fun getListItemOffsets(position: Int, outRect: Rect, parent: RecyclerView) {
        val itemCount = parent.adapter?.itemCount ?: return

        outRect.left = spacing
        outRect.right = spacing

        when (position) {
            0 -> {
                outRect.top = spacing
                outRect.bottom = halfSpacing
            }
            itemCount - 1 -> {
                outRect.top = halfSpacing
                outRect.bottom = spacing
            }
            else -> {
                outRect.top = halfSpacing
                outRect.bottom = halfSpacing
            }
        }
    }
}