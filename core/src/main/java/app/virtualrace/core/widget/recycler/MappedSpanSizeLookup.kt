package app.virtualrace.core.widget.recycler

import androidx.recyclerview.widget.GridLayoutManager
import app.virtualrace.core.presentation.provider.Provider

internal class MappedSpanSizeLookup(
        private val provider: Provider<*>,
        private val spans: Map<Class<*>, Int>,
        private val defaultSpan: Int = DEFAULT_SPAN
) : GridLayoutManager.SpanSizeLookup() {
    companion object {
        const val DEFAULT_SPAN = 1
    }

    override fun getSpanSize(position: Int): Int {
        val item = provider[position] ?: throw IllegalArgumentException()
        val itemClass = item::class.java
        var span = spans[itemClass]
        if (span == null) {
            val itemSuperType = spans.keys.firstOrNull { it.isAssignableFrom(itemClass) }
            if (itemSuperType != null) {
                span = spans[itemSuperType]
            }
        }

        return span ?: defaultSpan
    }
}

fun spanSizeLookupOf(
        provider: Provider<*>,
        defaultSpan: Int,
        spans: Map<Class<*>, Int>
): GridLayoutManager.SpanSizeLookup = MappedSpanSizeLookup(provider, spans, defaultSpan)

fun spanSizeLookupOf(
        provider: Provider<*>,
        defaultSpan: Int,
        vararg spans: Pair<Class<*>, Int>
) = spanSizeLookupOf(provider, defaultSpan, mapOf(*spans))