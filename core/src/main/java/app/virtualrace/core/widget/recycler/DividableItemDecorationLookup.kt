package app.virtualrace.core.widget.recycler

import app.virtualrace.core.presentation.provider.Provider

internal class DividableItemDecorationLookup<T : Dividable>(
        private val provider: Provider<T>
) : ItemDecorationLookup {
    override fun drawDividerForPosition(position: Int): Boolean {
        return provider[position].hasDivider
    }
}

fun <T : Dividable> Provider<T>.dividableLookup(): ItemDecorationLookup =
        DividableItemDecorationLookup(this)