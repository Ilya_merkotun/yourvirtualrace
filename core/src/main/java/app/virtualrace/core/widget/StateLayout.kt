package app.virtualrace.core.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.IntDef
import androidx.core.view.isVisible
import app.virtualrace.core.R


class StateLayout @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    @State
    private var state: Int
    private var customState: String? = null

    init {
        val ta = context.theme.obtainStyledAttributes(attrs, R.styleable.StateLayout, 0, 0)

        state = ta.getInt(R.styleable.StateLayout_state, State.NONE)
        customState = if (state == State.CUSTOM) ta.getString(R.styleable.StateLayout_custom_state) else null

        ta.recycle()
    }

    @State
    fun getState() = state

    fun setState(@State state: Int) {
        this.state = state
        customState = null

        updateViewsVisibility()
    }

    fun getCustomState() = customState

    fun setCustomState(customState: String) {
        state = State.CUSTOM
        this.customState = customState

        updateViewsVisibility()
    }

    override fun addView(child: View, index: Int, params: ViewGroup.LayoutParams) {
        val stateLayoutParams = params as LayoutParams
        if (stateLayoutParams.state != State.NONE) {
            super.addView(child, index, params)
            updateViewVisibility(child)
        }
    }

    private fun updateViewsVisibility() {
        var child: View
        for (index in 0 until childCount) {
            child = getChildAt(index)

            updateViewVisibility(child)
        }
    }

    private fun updateViewVisibility(view: View) {
        val params = view.layoutParams as LayoutParams
        view.isVisible = params.state == state && params.name == customState
    }

    override fun generateDefaultLayoutParams(): FrameLayout.LayoutParams =
            LayoutParams(context, null)

    override fun generateLayoutParams(attrs: AttributeSet?): FrameLayout.LayoutParams =
            LayoutParams(context, attrs)

    private class LayoutParams(c: Context, attrs: AttributeSet?) : FrameLayout.LayoutParams(c, attrs) {
        @State
        val state: Int
        val name: String?

        init {
            val ta = c.theme.obtainStyledAttributes(attrs, R.styleable.StateLayout_LayoutParamsAttributes, 0, 0)

            state = ta.getInt(R.styleable.StateLayout_LayoutParamsAttributes_layout_state, State.NONE)
            name = if (state == State.CUSTOM) {
                ta.getString(R.styleable.StateLayout_LayoutParamsAttributes_layout_custom_state)
            } else null
        }
    }

    @IntDef(State.EMPTY, State.PROGRESS, State.CONTENT, State.CUSTOM, State.NONE)
    @Retention(AnnotationRetention.SOURCE)
    annotation class State {
        companion object {
            const val EMPTY = 0
            const val PROGRESS = 1
            const val CONTENT = 2
            const val CUSTOM = -1
            const val NONE = -2
        }
    }
}