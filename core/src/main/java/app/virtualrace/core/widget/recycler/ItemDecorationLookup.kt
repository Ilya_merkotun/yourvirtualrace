package app.virtualrace.core.widget.recycler

interface ItemDecorationLookup {
    fun drawDividerForPosition(position: Int): Boolean = true
}