package app.virtualrace.core.widget.recycler

interface Dividable {
    val hasDivider: Boolean get() = true
}